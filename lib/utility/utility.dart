import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

//region Base64ToImage
Image base64ToImage(String base64String) {
  //Convert from base64 string to image
  return Image.memory(base64Decode(base64String));
}
//endregion

//region base64ToFile
Uint8List base64ToFile(String base64String) {
  //Convert from base64 string to file
  return base64Decode(base64String);
}
//endregion

//region FileToBase64
String fileToBase64(File file) {
  Uint8List bytes = file.readAsBytesSync();
  return base64Encode(bytes);
}
//endregion

//region BytesToBase64
String bytesToBase64(Uint8List bytes) {
  return base64Encode(bytes);
}
//endregion

//region getIcon
IconData getIcon(String iconName) {

  if (iconName == "Home") return LineAwesomeIcons.home;
  if (iconName == "ShippingFast") return LineAwesomeIcons.shipping_fast;
  if (iconName == "Clock") return LineAwesomeIcons.clock;
  if (iconName == "UserEdit") return LineAwesomeIcons.user_edit;
  if (iconName == "Lock") return Icons.lock_outline;
  if (iconName == "SignOut") return LineAwesomeIcons.alternate_sign_out;
  if (iconName == "User") return LineAwesomeIcons.user;
  if (iconName == "UserTie") return LineAwesomeIcons.user_tie;
  if (iconName == "HandPointingUp") return LineAwesomeIcons.hand_pointing_up;
  if (iconName == "CheckCircle") return LineAwesomeIcons.check_circle;
  if (iconName == "TelegramPlane") return LineAwesomeIcons.telegram_plane;
  if (iconName == "Search") return LineAwesomeIcons.search;
  if (iconName == "Settings") return LineAwesomeIcons.cog;
  if (iconName == "Face") return Icons.face_retouching_natural;
  if (iconName == "Edit") return LineAwesomeIcons.edit;
  if (iconName == "Save") return LineAwesomeIcons.save;
  if (iconName == "OilCan") return LineAwesomeIcons.oil_can;

  return LineAwesomeIcons.hand_pointing_up;
}
//endregion
