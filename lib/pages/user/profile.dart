import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:iterminal/app_config.dart';
import 'package:iterminal/component/buttons.dart';
import 'package:iterminal/utility/utility.dart';

class ProfilePage extends StatefulWidget {
  //Create state
  @override
  ProfilePageState createState() => ProfilePageState();
}

class ProfilePageState extends State<ProfilePage> {
  bool isOilLicenseNo = false;
  bool isGasLicenseNo = false;

  //region InitState
  @override
  void initState() {
    super.initState();

    //Check oil license
    if (TempData.userInfo.oilLicenseNo.isNotEmpty) {
      isOilLicenseNo = true;
    }

    //Check gas license
    if (TempData.userInfo.gasLicenseNo.isNotEmpty) {
      isGasLicenseNo = true;
    }
  }
  //endregion

  //WidgetView
  @override
  Widget build(BuildContext context) {
    //region ProfileContent
    var profileContent = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.person,
          size: 35.0,
        ),
        SizedBox(width: 15),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "ข้อมูลผู้ใช้งาน",
                style: TextStyle(
                    fontSize: App.fontSize,
                    fontWeight: FontWeight.bold,
                    color: App.fontColor),
              ),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "รหัส : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.driverNo,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "ชื่อ : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.fullName,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "เลขบัตรประชาชน : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.idCard,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "วันหมดอายุ : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.idCardExpDate,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "ผู้ขนส่ง : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.vendorName,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
            ],
          ),
        )
      ],
    );
    //endregion

    //region OilLicenseContent
    var oilLicenseContent = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.assignment,
          size: 35.0,
        ),
        SizedBox(width: 15),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "ใบอนุญาต ธพ.น.2",
                style: TextStyle(
                    fontSize: App.fontSize,
                    fontWeight: FontWeight.bold,
                    color: App.fontColor),
              ),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "เลขที่ : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.oilLicenseNo,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "วันที่อนุญาต : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.oilLicenseDate,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "วันหมดอายุ : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.oilLicenseExpDate,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
            ],
          ),
        )
      ],
    );
    //endregion

    //region GasLicenseContent
    var gasLicenseContent = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.assignment,
          size: 35.0,
        ),
        SizedBox(width: 15),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "ใบอนุญาต ธพ.ก.2",
                style: TextStyle(
                    fontSize: App.fontSize,
                    fontWeight: FontWeight.bold,
                    color: App.fontColor),
              ),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "เลขที่ : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.gasLicenseNo,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "วันที่อนุญาต : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.gasLicenseDate,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "วันหมดอายุ : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.gasLicenseExpDate,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
            ],
          ),
        )
      ],
    );
    //endregion

    //region ProfileImage
    var profileImage = Container(
      child: Positioned(
        top: 25.0,
        child: Container(
          child: CircleAvatar(
            radius: 50,
            backgroundImage: TempData.userInfo.driverImage.isNotEmpty
                ? base64ToImage(TempData.userInfo.driverImage).image
                : AssetImage("assets/images/profile.png"),
            backgroundColor: Colors.transparent,
          ),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: new Border.all(
              color: Colors.blueGrey,
              width: 1.0,
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 3,
                blurRadius: 3,
                //offset: Offset(0, 3),
              ),
            ],
          ),
        ),
      ),
    );
    //endregion

    //region MainContentView
    return Scaffold(
      appBar: AppBar(
        leading: AppBarBackIcon(),
        title: Text("ข้อมูลส่วนตัว"),
      ),
      body: SingleChildScrollView(
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
//            Container(
//              color: Colors.blue[500],
//              height: MediaQuery.of(context).size.height / 6,
//            ),
            Padding(
                padding: EdgeInsets.only(top: 75.0),
                child: Container(
                  //height: MediaQuery.of(context).size.height,
//                  decoration: BoxDecoration(
//                      color: Colors.white,
//                      borderRadius: BorderRadius.all(Radius.circular(20)),
//                      border: Border.all(color: Colors.grey, width: 0.2)),
//                  child: Card(
//                    //color: Colors.white70,
//                    shape: RoundedRectangleBorder(
//                      //side: BorderSide(color: Colors.white70, width: 1),
//                      borderRadius: BorderRadius.circular(10),
//                    ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 75.0),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        child: profileContent,
                      ),
                      SizedBox(height: 15),
                      if (isOilLicenseNo)
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          child: Column(
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                  border: Border(
                                    bottom: BorderSide(
                                      color: Colors.blue,
                                      width: 1.0,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 15),
                              oilLicenseContent,
                              SizedBox(height: 15),
                            ],
                          ),
                        ),
                      if (isGasLicenseNo)
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 10.0),
                          child: Column(
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                  border: Border(
                                    bottom: BorderSide(
                                      color: Colors.blue,
                                      width: 1.0,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 15),
                              gasLicenseContent,
                              SizedBox(height: 15),
                            ],
                          ),
                        ),
                    ],
                  ),
                  //),
                )),
            profileImage
          ],
        ),
      ),
    );
    //endregion
  }
}
