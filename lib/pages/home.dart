import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:iterminal/app_config.dart';
import 'package:iterminal/pages/login.dart';
import 'package:iterminal/pages/user/profile.dart';
import 'package:iterminal/pages/depot_list.dart';
import 'package:iterminal/component/dialogs.dart';
import 'package:iterminal/utility/utility.dart';
import 'package:iterminal/pages/settings/setting_list.dart';

class HomePage extends StatefulWidget {
  //Create state
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  //region InitState
  @override
  void initState() {
    super.initState();
  }

  //endregion

  //WidgetView
  @override
  Widget build(BuildContext context) {
    //region HeaderContent
    var headerContent = Padding(
      padding: EdgeInsets.symmetric(vertical: 65, horizontal: 20),
      child: Column(
        children: <Widget>[
          Container(
            child: CircleAvatar(
              radius: 40,
              backgroundImage: TempData.userInfo.driverImage.isNotEmpty
                  ? base64ToImage(TempData.userInfo.driverImage).image
                  : AssetImage("assets/images/profile.png"),
              backgroundColor: Colors.transparent,
            ),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: new Border.all(
                color: Colors.blueGrey,
                width: 1.0,
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.blueGrey.withOpacity(0.5),
                  spreadRadius: 3,
                  blurRadius: 3,
                  //offset: Offset(0, 3),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Text(
            "คุณ" + TempData.userInfo.fullName,
            style: TextStyle(
              color: Colors.white,
              fontSize: App.fontSize,
            ),
          ),
        ],
      ),
    );
    //endregion

    //region MenuContent
    var menuContent = GridView.count(
      crossAxisCount: 3,
      children: List<Widget>.generate(Menus.mainMenuList.length, (index) {
        return InkWell(
          child: Card(
            //color: Colors.blueGrey[600],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            elevation: 3,
            child: Padding(
              padding: EdgeInsets.all(5.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    getIcon(Menus.mainMenuList[index].menuIcon),
                    size: 40.0,
                    color: App.fontColor,
                  ),
                  SizedBox(height: 10.0),
                  Text(
                    Menus.mainMenuList[index].menuName,
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(color: App.fontColor, fontSize: App.fontSize),
                  ),
                ],
              ),
            ),
          ),
          onTap: () => {onMenuSelected(Menus.mainMenuList[index])},
        );
      }),
    );
    //endregion

    //region MainContentView
    return Scaffold(
      backgroundColor: Colors.blue[500],
      body: Stack(
        children: <Widget>[
//          Container(
//            //color: Colors.white70,
//            //height: MediaQuery.of(context).size.height / 3,
//          ),
          headerContent,
          Padding(
            padding: EdgeInsets.only(top: 230.0),
            child: Card(
              color: Colors.white70,
              //clipBehavior: Clip.antiAlias,
              margin: EdgeInsets.all(0),
              shape: RoundedRectangleBorder(
                //side: BorderSide(color: Colors.white70, width: 1),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
              ),
              child: Column(
                children: <Widget>[
                  Container(
                    height: 55.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10.0),
                          topRight: Radius.circular(10.0)),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Row(
                          //mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.home,
                              size: 25,
                              color: App.fontColor,
                            ),
                            SizedBox(width: 5.0),
                            Text(
                              "เลือกรายการ",
                              style: TextStyle(
                                  fontSize: App.fontSize,
                                  fontWeight: FontWeight.bold,
                                  color: App.fontColor),
                            ),
                          ]),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      child: menuContent,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
    //endregion
  }

  //region OnMenuSelected
  void onMenuSelected(menuSelected) async {
    try {
      Menus.isActionID = menuSelected.menuId;

      if (menuSelected.menuId == Menus.menuDeliveryID || menuSelected.menuId == Menus.menuQueueID) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => DepotListPage()));
      } else if (menuSelected.menuId == Menus.menuProfileID) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => ProfilePage()));
      } else if (menuSelected.menuId == Menus.menuSettingID) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => SettingListPage(menuIdSelected: menuSelected.menuId,)));
      } else {
        var isAction = await alertConfirm(context, SysMessage.ConfirmLogout);
        if (isAction) {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => LoginPage()));
        }
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

}
