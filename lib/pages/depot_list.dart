import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:iterminal/app_config.dart';
import 'package:iterminal/component/buttons.dart';
import 'package:iterminal/models/depot_model.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:iterminal/pages/vehicle_list.dart';
import 'package:iterminal/component/dialogs.dart';
import 'package:iterminal/services/depot_service.dart';
import 'package:iterminal/component/input_field.dart';
import 'package:iterminal/services/driver_service.dart';
import 'package:iterminal/models/driver_model.dart';

class DepotListPage extends StatefulWidget {
  //Create state
  @override
  DepotListPageState createState() => DepotListPageState();
}

class DepotListPageState extends State<DepotListPage> {
  List<DepotInfo> depotListAll = [];
  List<DepotInfo> depotList = [];
  bool isSearching = false;

  //region InitState
  @override
  void initState() {
    super.initState();

    getDepotList();
  }

  //endregion

  //WidgetView
  @override
  Widget build(BuildContext context) {
    //region MainContentView
    return Scaffold(
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        //backgroundColor: Colors.indigoAccent,
        leading: AppBarBackIcon(),
        titleSpacing: 5.0,
        title: isSearching
            ? InputFieldExtra(
                icon: LineAwesomeIcons.search,
                textHint: "ค้นหา",
                onChange: (value) => {
                      onSearchTextChange(value),
                    })
            : Text("เลือกคลัง"),
        actions: <Widget>[
          isSearching
              ? IconButton(
                  icon: Icon(
                    Icons.cancel,
                    color: Colors.white,
                    size: 30,
                  ),
                  onPressed: () => {onCancelSearchClick()},
                )
              : IconButton(
                  icon: Icon(
                    Icons.search,
                    color: Colors.white,
                    size: 30,
                  ),
                  onPressed: () => {onSearchClick()},
                )
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              itemCount: depotList.length,
              itemBuilder: (context, index) {
                return InkWell(
                  child: Card(
                    margin: const EdgeInsets.all(5.0),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.business,
                            size: 35.0,
                          ),
                          SizedBox(width: 15),
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  depotList[index].depotId,
                                  style: TextStyle(
                                      fontSize: App.fontSize,
                                      color: App.fontColor),
                                ),
                                SizedBox(height: 5.0),
                                Text(
                                  depotList[index].depotName,
                                  style: TextStyle(
                                      fontSize: App.fontSize,
                                      color: App.fontColor),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  onTap: () => {onDepotSelected(depotList[index])},
                );
              },
            ),
          ),
        ],
      ),
    );
    //endregion
  }

  //region GetDepotList
  void getDepotList() async {
    try {
      var data = await depotService.getDepotList(TempData.userInfo.driverNo);
      if (SysMessage.errorMsg.isEmpty) {
        setState(() {
          depotListAll = data.depotList;
          depotList = data.depotList;
        });
      } else {
        await alertError(context, SysMessage.errorMsg);
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region OnSearchClick
  void onSearchClick() async {
    try {
      setState(() {
        isSearching = true;
      });
    } catch (ex) {
      alertError(context, ex);
    }
  }

  //endregion

  //region OnCancelSearchClick
  void onCancelSearchClick() async {
    try {
      setState(() {
        isSearching = false;
        depotList = depotListAll;
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region OnSearchTextChange
  void onSearchTextChange(value) async {
    try {
      setState(() {
        depotList = depotListAll
            .where((item) =>
                item.depotId
                    .toString()
                    .toLowerCase()
                    .contains(value.toLowerCase()) ||
                item.depotName
                    .toString()
                    .toLowerCase()
                    .contains(value.toLowerCase()))
            .toList();
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region OnDepotSelected
  void onDepotSelected(depotSelected) async {
    try {
      var inputData = new CheckDriverData();
      inputData.driverNo = TempData.userInfo.driverNo;
      inputData.driverName = TempData.userInfo.firstName;
      inputData.depotType = depotSelected.depotTypeId;
      inputData.sequent = 1;

      await driverService.checkDriverData(inputData);
      if (SysMessage.errorMsg.isEmpty) {
        //Keep selected depot to temp data
        TempData.depotInfo = depotSelected;

        Navigator.push(context,
            MaterialPageRoute(builder: (context) => VehicleListPage()));
      } else {
        await alertError(context, SysMessage.errorMsg);
      }
    } catch (ex) {
      await alertError(context, "ไลเซนส์หมดอายุ");
    }
  }

//endregion

}
