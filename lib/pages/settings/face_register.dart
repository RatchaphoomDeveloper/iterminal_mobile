import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path/path.dart' as path;
import 'package:intl/intl.dart';
import 'package:flutter/cupertino.dart';
import 'package:iterminal/models/login_model.dart';
import 'package:iterminal/services/login_service.dart';
import 'package:image_picker/image_picker.dart';
import 'package:iterminal/app_config.dart';
import 'package:iterminal/component/dialogs.dart';
import 'package:iterminal/component/buttons.dart';
import 'package:iterminal/utility/utility.dart';
import 'package:iterminal/component/input_field.dart';

class FaceRegisterPage extends StatefulWidget {
  //Create state
  @override
  FaceRegisterPageState createState() => FaceRegisterPageState();
}

class FaceRegisterPageState extends State<FaceRegisterPage> {
  var txtDriverNo = new TextEditingController();
  var txtNameTh = new TextEditingController();
  var txtNameEng = new TextEditingController();
  var txtFileName = new TextEditingController();
  var picker = new ImagePicker();
  var base64Image = "";

  //region InitState
  @override
  void initState() {
    super.initState();

    txtDriverNo.text = TempData.userInfo.driverNo;
    txtNameTh.text = TempData.userInfo.fullName;
  }

  //endregion

  //WidgetView
  @override
  Widget build(BuildContext context) {
    //region MainContentView
    return Scaffold(
      appBar: AppBar(
        leading: AppBarBackIcon(),
        title: Text("เพิ่มข้อมูลใบหน้า"),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(5.0),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              //mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "รหัสพนักงาน",
                        style: TextStyle(fontSize: App.fontSize, color: App.fontColor),
                      ),
                      Text(
                        " *",
                        style: TextStyle(fontSize: App.fontSize, color: Colors.red),
                      ),
                    ],
                  ),
                ),
                InputField(
                  fieldName: txtDriverNo,
                  fontSize: App.fontSize,
                  bgColor: Colors.blueGrey[300],
                  readOnly: true,
                ),
                SizedBox(height: 5.0),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "ชื่อ (ภาษาไทย)",
                        style: TextStyle(fontSize: App.fontSize, color: App.fontColor),
                      ),
                      Text(
                        " *",
                        style: TextStyle(fontSize: App.fontSize, color: Colors.red),
                      ),
                    ],
                  ),
                ),
                InputField(
                  fieldName: txtNameTh,
                  fontSize: App.fontSize,
                  bgColor: Colors.blueGrey[300],
                  readOnly: true,
                ),
                SizedBox(height: 5.0,),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "ชื่อ (ภาษาอังกฤษ)",
                        style: TextStyle(fontSize: App.fontSize, color: App.fontColor),
                      ),
                      Text(
                        " *",
                        style: TextStyle(fontSize: App.fontSize, color: Colors.red),
                      ),
                    ],
                  ),
                ),
                InputField(
                  fieldName: txtNameEng,
                  fontSize: App.fontSize,
                  bgColor: Colors.blueGrey[300],
                  //textHint: "กรุณาระบุ..",
                ),
                SizedBox(height: 5.0,),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "รูปภาพ",
                        style: TextStyle(fontSize: App.fontSize, color: App.fontColor),
                      ),
                      Text(
                        " *",
                        style: TextStyle(fontSize: App.fontSize, color: Colors.red),
                      ),
                    ],
                  ),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: InputField(
                        fieldName: txtFileName,
                        fontSize: App.fontSize,
                        bgColor: Colors.blueGrey[300],
                        readOnly: true,
                      ),
                    ),
                    MaterialButton(
                      color: Colors.green,
                      textColor: Colors.white,
                      //height: 47.0,
                      child: Icon(
                        Icons.camera_alt,
                        size: 28,
                      ),
                      shape: CircleBorder(),
                      padding: EdgeInsets.all(10),
                      onPressed: () => {takePhoto()},
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              if (Menus.registerFace != null)
                Flexible(
                  child: Button(
                    icon: getIcon(Menus.registerFace.menuIcon),
                    text: Menus.registerFace.menuName,
                    onClick: () => {faceRegister()},
                  ),
                ),
            ],
          ),
        ),
        color: Colors.transparent,
        elevation: 0,
      ),
    );
    //endregion
  }

  //region TakePhoto
  void takePhoto() async {
    try {
      //var filePicker = await picker.getImage(source: ImageSource.gallery); //Pick file
      //var videoPicker = await picker.getVideo(source: ImageSource.camera); //Take video

      //Take photo
      var filePicker = await picker.getImage(
          source: ImageSource.camera,
          maxHeight: 600,
          maxWidth: 800,
          imageQuality: 70);

      if (filePicker != null) {
        //Rename of file
        String dir = path.dirname(filePicker.path);
        String dateNow = DateFormat('yyyyMMdd_HHmmss').format(DateTime.now());
        String newFileName = TempData.userInfo.driverNo + "_" + dateNow + ".jpg";
        String newPath = path.join(dir, newFileName);

        //Get file
        File file = await File(filePicker.path).copy(newPath);
        txtFileName.text = path.basename(file.path);
        base64Image = fileToBase64(file);
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region FaceRegister
  void faceRegister() async {
    try {
      var faceData = new FaceDataModel();
      faceData.driverNo = txtDriverNo.text;
      faceData.driverName = txtNameEng.text;
      faceData.base64FaceImage = base64Image;

      var statusMsg = await loginService.faceRegister(faceData);
      if (SysMessage.errorMsg.isEmpty) {
        await alertSuccess(context, statusMsg);
        txtFileName.text = "";
        base64Image = "";
      } else {
        await alertError(context, SysMessage.errorMsg);
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

}
