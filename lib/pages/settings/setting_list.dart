import 'package:flutter/material.dart';
import 'package:iterminal/app_config.dart';
import 'package:iterminal/utility/utility.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:iterminal/component/dialogs.dart';
import 'package:iterminal/pages/settings/face_register.dart';
import 'package:iterminal/component/buttons.dart';

class SettingListPage extends StatefulWidget {
  final int menuIdSelected;

  SettingListPage({this.menuIdSelected});

  //Create state
  @override
  SettingListPageState createState() => SettingListPageState();
}

class SettingListPageState extends State<SettingListPage> {
  //region InitState
  @override
  void initState() {
    super.initState();
  }

  //endregion

  //WidgetView
  @override
  Widget build(BuildContext context) {
    //region MainContentView
    return Scaffold(
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        leading: AppBarBackIcon(),
        title: Text("การตั้งค่า"),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              itemCount: Menus.settingMenuList.length,
              itemBuilder: (context, index) {
                return InkWell(
                  child: Card(
                    margin: const EdgeInsets.all(5.0),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            getIcon(Menus.settingMenuList[index].menuIcon),
                            size: 35.0,
                          ),
                          SizedBox(width: 15),
                          Flexible(
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  Menus.settingMenuList[index].menuName,
                                  style: TextStyle(
                                      fontSize: App.fontSize,
                                      color: App.fontColor),
                                ),
                                Spacer(),
                                Icon(
                                  Icons.keyboard_arrow_right, color: App.fontColor,
                                  //size: this.iconSize,
                                  //color: this.iconColor,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  onTap: () => {onListMenuSelected(Menus.settingMenuList[index])},
                );
              },
            ),
          ),
        ],
      ),
    );
    //endregion
  }

  //region OnListMenuSelected
  void onListMenuSelected(settingsMenuSelected) async {
    try {
      if (settingsMenuSelected.menuId == Menus.menuAddFaceID) {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => FaceRegisterPage()));
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

}
