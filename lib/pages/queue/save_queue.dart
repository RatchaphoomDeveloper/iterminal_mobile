import 'package:flutter/material.dart';
import 'package:iterminal/app_config.dart';
import 'package:iterminal/component/dialogs.dart';
import 'package:iterminal/component/buttons.dart';
import 'package:iterminal/models/queue_model.dart';
import 'package:iterminal/pages/home.dart';
import 'package:iterminal/services/queue_service.dart';
import 'package:iterminal/utility/utility.dart';

class SaveQueuePage extends StatefulWidget {
  //Create state
  @override
  SaveQueuePageState createState() => SaveQueuePageState();
}

class SaveQueuePageState extends State<SaveQueuePage> {
  //region InitState
  @override
  void initState() {
    super.initState();
  }

  //endregion

  //WidgetView
  @override
  Widget build(BuildContext context) {
    //region DepotContent
    var depotContent = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.business,
          size: 35.0,
        ),
        SizedBox(width: 15),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "ข้อมูลคลัง",
                style: TextStyle(
                    fontSize: App.fontSize,
                    fontWeight: FontWeight.bold,
                    color: App.fontColor),
              ),
              SizedBox(height: 5.0),
              Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "รหัส : ",
                      style: TextStyle(
                          fontSize: App.fontSize, color: App.fontColor),
                    ),
                    Flexible(
                        child: Text(
                      TempData.depotInfo.depotId,
                      style:
                          TextStyle(fontSize: App.fontSize, color: Colors.blue),
                    ))
                  ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "คลัง : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.depotInfo.depotName,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
            ],
          ),
        ),
      ],
    );
    //endregion

    //region VehicleContent
    var vehicleContent = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.directions_bus,
          size: 35.0,
        ),
        SizedBox(width: 15),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "ข้อมูลรถ",
                style: TextStyle(
                    fontSize: App.fontSize,
                    fontWeight: FontWeight.bold,
                    color: App.fontColor),
              ),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "ทะเบียนหัว : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.vehicleInfo.vehicleNo,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "ทะเบียนหาง : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.vehicleTuInfo.vehicleTuNo,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "ประเภท : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.vehicleInfo.vehicleTypeName,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "จำนวนช่อง : ",
                  style:
                  TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                      TempData.tuCompList.length.toString(),
                      style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                    ))
              ]),
            ],
          ),
        ),
      ],
    );
    //endregion

    //region DriverContent
    var driverConten = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.person,
          size: 35.0,
        ),
        SizedBox(width: 15),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "พนักงานขับรถ",
                style: TextStyle(
                    fontSize: App.fontSize,
                    fontWeight: FontWeight.bold,
                    color: App.fontColor),
              ),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "รหัส : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.driverNo,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "ชื่อ : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.fullName,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "เลขบัตรประชาชน : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.idCard,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "ผู้ขนส่ง : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.vendorName,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
            ],
          ),
        )
      ],
    );
    //endregion

    //region MainContentView
    return Scaffold(
      appBar: AppBar(
        leading: AppBarBackIcon(),
        title: Text("ลงคิว"),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(5.0),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(children: <Widget>[
              depotContent,
              SizedBox(height: 15),
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.blue,
                      width: 1.0,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
              vehicleContent,
              SizedBox(height: 15),
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.blue,
                      width: 1.0,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
              driverConten,
              SizedBox(height: 15),
            ]),
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              if (Menus.confirmQueue != null)
                Flexible(
                  child: Button(
                    icon: getIcon(Menus.confirmQueue.menuIcon),
                    text: Menus.confirmQueue.menuName,
                    onClick: () =>
                    {saveQueue()},
                  ),
                ),
            ],
          ),
        ),
        color: Colors.transparent,
        elevation: 0,
      ),
    );
    //endregion
  }

  //region SaveQueue
  void saveQueue() async {
    try {
      var inputData = new SaveQueueModel();
      inputData.driverNo = TempData.userInfo.driverNo;
      inputData.idCard = TempData.userInfo.idCard;
      inputData.firstName = TempData.userInfo.firstName;
      inputData.lastName = TempData.userInfo.lastName;
      inputData.vendorCode = TempData.userInfo.vendorCode;
      inputData.createBy = TempData.userInfo.driverNo;
      inputData.vehicleNo = TempData.vehicleInfo.vehicleNo;
      inputData.vehicleTuNo = TempData.vehicleTuInfo.vehicleTuNo;
      inputData.depot = TempData.depotInfo.depotId;

      //Save queue
      var statusMsg = await queueService.saveQueue(inputData);
      if (SysMessage.errorMsg.isEmpty) {
        await alertSuccess(context, statusMsg);
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomePage()));
      } else {
        await alertError(context, SysMessage.errorMsg);
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

}
