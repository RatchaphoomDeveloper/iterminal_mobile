import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:iterminal/app_config.dart';
import 'package:iterminal/component/buttons.dart';
import 'package:iterminal/models/vehicle_model.dart';
import 'package:iterminal/pages/queue/save_queue.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:iterminal/component/dialogs.dart';
import 'package:iterminal/services/vehicle_service.dart';
import 'package:iterminal/component/input_field.dart';
import 'package:iterminal/pages/delivery/delivery.dart';

class VehicleListPage extends StatefulWidget {
  //Create state
  @override
  VehicleListPateState createState() => VehicleListPateState();
}

class VehicleListPateState extends State<VehicleListPage> {
  List<VehicleInfo> vehicleListAll = [];
  List<VehicleInfo> vehicleList = [];
  bool isSearching = false;

  //region InitState
  @override
  void initState() {
    super.initState();

    getVehicleList();
  }

  //endregion

  //WidgetView
  @override
  Widget build(BuildContext context) {
    //region MainContentView
    return Scaffold(
      appBar: AppBar(
        leading: AppBarBackIcon(),
        titleSpacing: 5.0,
        title: isSearching
            ? InputFieldExtra(
                icon: LineAwesomeIcons.search,
                textHint: "ค้นหา",
                onChange: (value) => {
                      onSearchTextChange(value),
                    })
            : Text("เลือกทะเบียน"),
        actions: <Widget>[
          isSearching
              ? IconButton(
                  icon: Icon(
                    Icons.cancel,
                    color: Colors.white,
                    size: 30,
                  ),
                  onPressed: () => {onCancelSearchClick()},
                )
              : IconButton(
                  icon: Icon(
                    Icons.search,
                    color: Colors.white,
                    size: 30,
                  ),
                  onPressed: () => {onSearchClick()},
                )
        ],
        //automaticallyImplyLeading: false,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              itemCount: vehicleList.length,
              itemBuilder: (context, index) {
                return InkWell(
                  child: Card(
                    margin: const EdgeInsets.all(5.0),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.directions_bus,
                            size: 35.0,
                          ),
                          SizedBox(width: 15),
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  vehicleList[index].vehicleNo,
                                  style: TextStyle(
                                      fontSize: App.fontSize,
                                      color: App.fontColor),
                                ),
                                SizedBox(height: 5.0),
                                Text(
                                  vehicleList[index].vehicleTypeName,
                                  style: TextStyle(
                                      fontSize: App.fontSize,
                                      color: App.fontColor),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  onTap: () => {onTruckSelected(vehicleList[index])},
                );
              },
            ),
          ),
        ],
      ),
    );
    //endregion
  }

  //region GetVehicleList
  void getVehicleList() async {
    try {
      var data = await vehicleService.getVehicleList(TempData.userInfo.vendorCode, TempData.depotInfo.depotId);
      if (SysMessage.errorMsg.isEmpty) {
        setState(() {
          vehicleListAll = data.vehicleList;
          vehicleList = data.vehicleList;
          //vehicleTuList = data.vehicleTranUnit;
        });
      } else {
        await alertError(context, SysMessage.errorMsg);
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region OnSearchClick
  void onSearchClick() async {
    try {
      setState(() {
        isSearching = true;
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region OnCancelSearchClick
  void onCancelSearchClick() async {
    try {
      setState(() {
        isSearching = false;
        vehicleList = vehicleListAll;
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region OnSearchTextChange
  void onSearchTextChange(value) async {
    try {
      setState(() {
        vehicleList = vehicleListAll
            .where((item) => item.vehicleNo
                .toString()
                .toLowerCase()
                .contains(value.toLowerCase()))
            .toList();
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region OnTruckSelected
  void onTruckSelected(vehicleSelected) async {
    try {
      var data = await vehicleService.getVehicleTu(
          vehicleSelected.vehicleNo,
          vehicleSelected.vehicleType,
          TempData.userInfo.vendorCode,
          TempData.depotInfo.depotId);

      if (SysMessage.errorMsg.isEmpty) {
        //Keep selected vehicle to temp data
        TempData.vehicleInfo = vehicleSelected;
        TempData.vehicleTuInfo = data.vehicleTuList[0];
        TempData.tuCompList = data.tuCompList;

        if (Menus.isActionID == Menus.menuDeliveryID) {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => DeliveryPage()));
        } else if (Menus.isActionID == Menus.menuQueueID) {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => SaveQueuePage()));
        } else {
          //Do nothing
        }
      } else {
        await alertError(context, SysMessage.errorMsg);
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

}
