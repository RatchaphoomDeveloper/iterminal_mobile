import 'dart:io';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:iterminal/app_config.dart';
import 'package:iterminal/utility/utility.dart';
import 'package:iterminal/pages/home.dart';
import 'package:iterminal/component/dialogs.dart';
import 'package:iterminal/component/buttons.dart';
import 'package:iterminal/component/input_field.dart';
import 'package:iterminal/models/login_model.dart';
import 'package:iterminal/services/login_service.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:flutter/services.dart';
import 'dart:io' show Platform;

class LoginPage extends StatefulWidget {
  //Create state
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  var idCard = ""; //= new TextEditingController();
  var password = ""; //= new TextEditingController();
  final picker = new ImagePicker();
  var base64Image = "";
  String assetPDFPath = "";
  String urlPDFPath = "";
  bool _authorizedOrNot = false;
  bool _canChekBioMetric = false;
  //region InitState
  @override
  void initState() {
    super.initState();
  }

  //WidgetView
  @override
  Widget build(BuildContext context) {
    //region MainContentView
    return Scaffold(
      backgroundColor: Colors.blue[500],
      //backgroundColor: Colors.lightBlue.withOpacity(0.8),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            InputFieldExtra(
              //fieldName: txtIDCard,
              onChange: (value) => {idCard = value},
              icon: Icons.person,
              textHint: "รหัสบัตรประชาชน",
              height: 45.0,
            ),
            SizedBox(height: 20),
            InputFieldExtra(
              //fieldName: txtPassword,
              onChange: (value) => {password = value},
              icon: Icons.lock,
              textHint: "รหัสผ่าน",
              typePassword: true,
              height: 45.0,
            ),
            SizedBox(height: 20),
            Button(
                text: "เข้าสู่ระบบ",
                icon: LineAwesomeIcons.alternate_sign_in,
                fontColor: Colors.blue,
                iconColor: Colors.blue,
                bgColor: Colors.white,
                height: 44,
                onClick: () => {getLogin(LoginType.UserPass)}),
            SizedBox(height: 20),
            Text(
              "Version " + App.AppVersion,
              style: TextStyle(color: Colors.white54, fontSize: 14),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.photo_camera,
          size: 30.0,
        ),
        backgroundColor: Colors.black38,
        // onPressed: takePhoto,
        onPressed: () => {takePhoto()},
      ),
    );
    //endregion
  }

  //web takephoto

  //region TakePhoto
  void takePhoto() async {
    try {
      //var filePicker = await picker.getImage(source: ImageSource.gallery); //Pick file
      //var videoPicker = await picker.getVideo(source: ImageSource.camera); //Take video

      //Take photo
      var filePicker = await picker.getImage(
          source: ImageSource.camera, maxHeight: 1800, maxWidth: 1800);

      if (filePicker != null) {
        if (Platform.isAndroid) {
          // Android-specific code
          File file = File(filePicker.path); //Get image file
          //FileImage image = FileImage(file); //Convert to type image

          //Get login
          base64Image = fileToBase64(file);
          getLogin(LoginType.Face);
        } else if (Platform.isIOS) {
          // iOS-specific code
          // _authorizeNow();
          File file = File(filePicker.path); //Get image file
          //FileImage image = FileImage(file); //Convert to type image

          //Get login
          base64Image = fileToBase64(file);
          getLogin(LoginType.Face);
        }

        //Get image from assets
        //ByteData bytes = await rootBundle.load('assets/images/P_GAME.jpg');
        //base64FaceImage = base64.encode(bytes.buffer.asUint8List());
        //getLogin(LoginType.Face);
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region GetLogin
  void getLogin(int loginType) async {
    var count = 0;
    var userData;
    var dataFlag = false;
    try {
      if (idCard.isNotEmpty &&
          password.isNotEmpty &&
          loginType != 2 &&
          idCard != "" &&
          password != "") {
        var inputData = new LoginModel();
        inputData.loginType = loginType;
        inputData.idCard = idCard;
        inputData.password = password;
        inputData.base64FaceImage = base64Image;

        var data = await loginService.getLogin(inputData);
        userData = data.userData[0];
        if (SysMessage.errorMsg.isEmpty && data.userData[0] != null) {
          //Get login data
          dataFlag = true;
          TempData.userInfo = data.userData[0];
          TempData.tokenKey = "Bearer " + TempData.userInfo.token;

          //Get menus
          Menus.mainMenuList = data.menuList
              .where((item) => item.menuLevel == 1 && item.menuType == "L")
              .toList();
          Menus.mainMenuList.sort((item1, item2) => item1.menuSequence
              .compareTo(item2.menuSequence)); //Sort by ascending
          //Menus.mainMenuList.sort((item1, item2) => item2.menuSequence.compareTo(item1.menuSequence)); //Sort by descending
          Menus.settingMenuList = data.menuList
              .where((item) =>
                  item.menunHeader == Menus.menuSettingID &&
                  item.menuType == "L")
              .toList();
          Menus.settingMenuList.sort((item1, item2) => item1.menuSequence
              .compareTo(item2.menuSequence)); //Sort by ascending
          Menus.sendByPtt = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuSendByPttID,
              orElse: () => null);
          Menus.getByCustomer = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuGetByCustomerID,
              orElse: () => null);
          Menus.confirmDriver = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuConfirmDriverID,
              orElse: () => null);
          Menus.manualComp = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuManualCompID,
              orElse: () => null);
          Menus.autoComp = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuAutoCompID,
              orElse: () => null);
          Menus.confirmComp = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuConfirmCompID,
              orElse: () => null);
          Menus.confirmQueue = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuConfirmQueueID,
              orElse: () => null);
          Menus.registerFace = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuRegisterFaceID,
              orElse: () => null);

          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => HomePage()));
        } else {
          await alertError(context, SysMessage.errorMsg);
        }
      }
      if (idCard.isEmpty && password.isEmpty && loginType != 1) {
        var inputData = new LoginModel();
        inputData.loginType = loginType;
        inputData.idCard = '';
        inputData.password = '';
        inputData.base64FaceImage = base64Image;

        var data = await loginService.getLogin(inputData);
        if (SysMessage.errorMsg.isEmpty && data.userData[0] != null) {
          dataFlag = true;
          //Get login data
          TempData.userInfo = data.userData[0];
          TempData.tokenKey = "Bearer " + TempData.userInfo.token;

          //Get menus
          Menus.mainMenuList = data.menuList
              .where((item) => item.menuLevel == 1 && item.menuType == "L")
              .toList();
          Menus.mainMenuList.sort((item1, item2) => item1.menuSequence
              .compareTo(item2.menuSequence)); //Sort by ascending
          //Menus.mainMenuList.sort((item1, item2) => item2.menuSequence.compareTo(item1.menuSequence)); //Sort by descending
          Menus.settingMenuList = data.menuList
              .where((item) =>
                  item.menunHeader == Menus.menuSettingID &&
                  item.menuType == "L")
              .toList();
          Menus.settingMenuList.sort((item1, item2) => item1.menuSequence
              .compareTo(item2.menuSequence)); //Sort by ascending
          Menus.sendByPtt = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuSendByPttID,
              orElse: () => null);
          Menus.getByCustomer = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuGetByCustomerID,
              orElse: () => null);
          Menus.confirmDriver = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuConfirmDriverID,
              orElse: () => null);
          Menus.manualComp = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuManualCompID,
              orElse: () => null);
          Menus.autoComp = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuAutoCompID,
              orElse: () => null);
          Menus.confirmComp = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuConfirmCompID,
              orElse: () => null);
          Menus.confirmQueue = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuConfirmQueueID,
              orElse: () => null);
          Menus.registerFace = data.menuList.firstWhere(
              (item) => item.menuId == Menus.menuRegisterFaceID,
              orElse: () => null);

          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => HomePage()));
        } else {
          await alertError(context, SysMessage.errorMsg);
        }
      } else {
        if (dataFlag == false) {
          await alertError(context, "ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง");
        }
        // if (count == 0 || count == 1) {
        //   if (SysMessage.errorMsg.toString() == "" && userData == null) {
        //     await alertError(context, "ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง");
        //   }
        // }
      }

      count++;
    } catch (ex) {
      await alertError(context, SysMessage.errorMsg);
    }
  }
//endregion
}
