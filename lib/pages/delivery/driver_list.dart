import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:iterminal/app_config.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:iterminal/component/dialogs.dart';
import 'package:iterminal/component/input_field.dart';
import 'package:iterminal/services/driver_service.dart';
import 'package:iterminal/models/driver_model.dart';
import 'package:iterminal/utility/utility.dart';
import 'package:iterminal/component/buttons.dart';
import 'package:iterminal/pages/delivery/delivery_data.dart';

class DriverListPage extends StatefulWidget {
//  final DepotInfo depotInfo;
//
//  DriverListPage({this.depotInfo});

  //Create state
  @override
  DriverListPageState createState() => DriverListPageState();
}

class DriverListPageState extends State<DriverListPage> {
  List<DriverInfo> driverListAll = [];
  List<DriverInfo> driverList = [];
  bool isSearching = false;

  //region InitState
  @override
  void initState() {
    super.initState();

    getDriverList();
  }

  //endregion

  //WidgetView
  @override
  Widget build(BuildContext context) {
    //region MainContentView
    return Scaffold(
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        //backgroundColor: Colors.indigoAccent,
        leading: AppBarBackIcon(),
        titleSpacing: 5.0,
        title: isSearching
            ? InputFieldExtra(
                icon: LineAwesomeIcons.search,
                textHint: "ค้นหา",
                onChange: (value) => {
                      onSearchTextChange(value),
                    })
            : Text("เพิ่ม พขร. (ถ้ามี)"),
        actions: <Widget>[
          isSearching
              ? IconButton(
                  icon: Icon(
                    Icons.cancel,
                    color: Colors.white,
                    size: 30,
                  ),
                  onPressed: () => {onCancelSearchClick()},
                )
              : IconButton(
                  icon: Icon(
                    Icons.search,
                    color: Colors.white,
                    size: 30,
                  ),
                  onPressed: () => {onSearchClick()},
                )
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              itemCount: driverList.length,
              itemBuilder: (context, index) {
                return Card(
                  margin: const EdgeInsets.all(5.0),
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      //crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          child: CircleAvatar(
                            radius: 30,
                            backgroundImage: driverList[index]
                                    .driverImage
                                    .isNotEmpty
                                ? base64ToImage(driverList[index].driverImage)
                                    .image
                                : AssetImage("assets/images/profile.png"),
                            backgroundColor: Colors.transparent,
                          ),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: new Border.all(
                              color: Colors.blueGrey,
                              width: 1.0,
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.blueGrey.withOpacity(0.5),
                                spreadRadius: 2,
                                blurRadius: 2,
                                //offset: Offset(0, 3),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(width: 20),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              driverList[index].driverNo,
                              style: TextStyle(
                                  fontSize: App.fontSize, color: App.fontColor),
                            ),
                            SizedBox(height: 5.0),
                            Text(
                              driverList[index].fullName,
                              style: TextStyle(
                                  fontSize: App.fontSize, color: App.fontColor),
                            ),
                          ],
                        ),
                        Spacer(),
                        InkWell(
                          child: Icon(
                            driverList[index].isSelected == true
                                ? Icons.check_circle
                                : Icons.add_circle,
                            size: 35.0,
                            color: driverList[index].isSelected == true
                                ? Colors.green
                                : Colors.orange,
                          ),
                          onTap: () => {onDriverSelected(index)},
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              if (Menus.confirmDriver != null)
                Flexible(
                  child: Button(
                    icon: getIcon(Menus.confirmDriver.menuIcon),
                    text: Menus.confirmDriver.menuName,
                    onClick: () => {onBtnConfirmDriverClick()},
                  ),
                ),
            ],
          ),
        ),
        color: Colors.transparent,
        elevation: 0,
      ),
    );
    //endregion
  }

  //region GetDriverList
  void getDriverList() async {
    try {
      var data = await driverService.getDriverList(
          TempData.userInfo.vendorCode, TempData.depotInfo.depotId);
      if (SysMessage.errorMsg.isEmpty) {
        setState(() {
          //Don't show login driver in list
          data.driverList.removeWhere(
              (item) => item.driverNo == TempData.userInfo.driverNo);
          driverListAll = data.driverList;
          driverList = data.driverList;
        });
      } else {
        await alertError(context, SysMessage.errorMsg);
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  // DateTime getDateTime(dt) {
  //   var dateSplit = dt.split(" ");
  //   var dateSplitd = dateSplit[0].split("/");
  //   DateTime dt = DateTime.parse(dateSplitd[2] +
  //               "-" +
  //               dateSplitd[1] +
  //               "-" +
  //               dateSplitd[0] +
  //               " " +
  //               dateSplit[1]);
  //   return dt;
  // }

  //endregion

  //region OnDriverSelected
  void onDriverSelected(index) async {
    try {
      if (driverList[index].isSelected == false) {
        //Check driver license
        var inputData = new CheckDriverData();
        inputData.driverNo = TempData.userInfo.driverNo;
        inputData.driverName = TempData.userInfo.firstName;
        inputData.depotType = TempData.depotInfo.depotTypeId;
        inputData.sequent = TempData.driverList.length + 1;
        bool gasFlag = false;
        bool oilFlag = false;

        var oilLicenseNoErr = "";
        var oilLicenseValidToErr = "";
        var cardExpireErr = "";

        var gasLicenseNoErr = "";
        var gasLicenseValidToErr = "";

        await driverService.checkDriverData(inputData);
        if (SysMessage.errorMsg.isEmpty) {
          if (TempData.depotInfo.depotTypeId == "1") {
            // CARD EXPIRED
            var dateSplit = driverList[index].cardExpideDate.split(" ");
            var dateSplitd = dateSplit[0].split("/");
            DateTime now = DateTime.now();
            DateTime dt = DateTime.parse(dateSplitd[2] +
                "-" +
                dateSplitd[1] +
                "-" +
                dateSplitd[0] +
                " " +
                dateSplit[1]);

            // OIL LICENSE VALID EXPIRED

            var dateSplitOilT = driverList[index].oillicenseValidTo.split(" ");
            var dateSplitOilD = dateSplitOilT[0].split("/");
            DateTime nowOil = new DateTime.now();
            DateTime dtOil = new DateTime.now();
            if (driverList[index].oillicenseValidTo != "") {
              dtOil = DateTime.parse(dateSplitOilD[2] +
                  "-" +
                  dateSplitOilD[1] +
                  "-" +
                  dateSplitOilD[0] +
                  " " +
                  dateSplitOilT[1]);
            }

            if (driverList[index].oillicenseNo == "") {
              oilLicenseNoErr =
                  "ไม่พบธพ.พ.2ผ ผู้ปฏิบัติงานถังขนส่งน้ำมัน";
              oilFlag = true;
            }if (now.isAfter(dt)) {
              cardExpireErr = "ใบขับขี่หมดอายุ";
              oilFlag = true;
            }if (nowOil.isAfter(dtOil) ||
                driverList[index].oillicenseValidTo == "") {
              oilLicenseValidToErr =
                  "ธพ.พ.2ผ ผู้ปฏิบัติงานถังขนส่งน้ำมันหมดอายุ";
              oilFlag = true;
            }
            if (oilFlag == true) {
              print(cardExpireErr +
                  " \n" +
                  oilLicenseNoErr +
                  " \n" +
                  oilLicenseValidToErr);
            }
          } else {
            // CARD EXPIRED
            var dateSplit = driverList[index].cardExpideDate.split(" ");
            var dateSplitd = dateSplit[0].split("/");
            DateTime now = DateTime.now();
            DateTime dt = DateTime.parse(dateSplitd[2] +
                "-" +
                dateSplitd[1] +
                "-" +
                dateSplitd[0] +
                " " +
                dateSplit[1]);

            // GAS LICENSE VALID EXPIRED
            var dateSplitGasT = driverList[index].gaslicenseValidTo.split(" ");
            var dateSplitGasD = dateSplitGasT[0].split("/");
            DateTime nowGas = new DateTime.now();
            DateTime dtGas = new DateTime.now();

            if (driverList[index].gaslicenseValidTo != "") {
              dtGas = DateTime.parse(dateSplitGasD[2] +
                  "-" +
                  dateSplitGasD[1] +
                  "-" +
                  dateSplitGasD[0] +
                  " " +
                  dateSplitGasT[1]);
            }

            if (driverList[index].gaslicenseNo == "") {
              gasLicenseNoErr =
                  "ไม่พบเอกสารธพ.พ.2ผ ผู้ปฏิบัติงานสถานีบรรจุก๊าซ";
              gasFlag = true;
            }if (now.isAfter(dt)) {
              cardExpireErr = "ใบขับขี่หมดอายุ";
              gasFlag = true;
            }if (nowGas.isAfter(dtGas) || driverList[index].gaslicenseValidTo == "") {
              gasLicenseValidToErr =
                  "เอกสารธพ.พ.2ผ ผู้ปฏิบัติงานสถานีบรรจุก๊าซหมดอายุ";
              gasFlag = true;
            }
            if (gasFlag == true) {
              print(cardExpireErr +
                  " \n" +
                  gasLicenseNoErr +
                  " \n" +
                  gasLicenseValidToErr);
              driverList[index].isSelected = false;
            }
          }
          if (oilFlag == false) {
            setState(() {
              //Add selected driver
              driverList[index].isSelected = true;
              TempData.driverList.add(driverList[index].driverNo);
            });
          } else {
            if (oilFlag == true) {
              driverList[index].isSelected = false;
              await alertError(
                  context,
                  cardExpireErr +
                      " \n" +
                      oilLicenseNoErr +
                      " \n" +
                      oilLicenseValidToErr);
            } else if (gasFlag == true) {
              driverList[index].isSelected = false;
              await alertError(
                  context,
                  cardExpireErr +
                      " \n" +
                      gasLicenseNoErr +
                      " \n" +
                      gasLicenseValidToErr);
            }
          }
        } else {
          await alertError(context, SysMessage.errorMsg);
        }
      } else {
        setState(() {
          //Remove selected driver
          driverList[index].isSelected = false;
          TempData.driverList.remove(driverList[index].driverNo);
        });
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region OnSearchClick
  void onSearchClick() async {
    try {
      setState(() {
        isSearching = true;
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region OnCancelSearchClick
  void onCancelSearchClick() async {
    try {
      setState(() {
        isSearching = false;
        driverList = driverListAll;
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region OnSearchTextChange
  void onSearchTextChange(value) async {
    try {
      setState(() {
        driverList = driverListAll
            .where((item) =>
                item.driverNo
                    .toString()
                    .toLowerCase()
                    .contains(value.toLowerCase()) ||
                item.fullName
                    .toString()
                    .toLowerCase()
                    .contains(value.toLowerCase()))
            .toList();
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

  //region OnBtnConfirmDriverClick
  void onBtnConfirmDriverClick() async {
    try {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => DeliveryDataPage()));
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

}
