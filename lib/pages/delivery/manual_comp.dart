import 'package:flutter/material.dart';
import 'package:iterminal/app_config.dart';
import "package:collection/collection.dart";
import 'package:iterminal/component/dialogs.dart';
import 'package:iterminal/component/input_field.dart';
import 'package:iterminal/models/order_model.dart';
import 'package:iterminal/pages/home.dart';
import 'package:iterminal/component/buttons.dart';
import 'package:iterminal/utility/utility.dart';
import 'package:iterminal/models/shipment_model.dart';
import 'package:iterminal/services/shipment_service.dart';
import 'dart:convert';

class ManualCompPage extends StatefulWidget {
  final List<OrderInfo> orderList;

  ManualCompPage({this.orderList});

  //Create state
  @override
  ManualCompPageState createState() => ManualCompPageState();
}

class ManualCompPageState extends State<ManualCompPage> {
  var txtQtyUsed = new TextEditingController();
  int availableQty = 0;
  List<ProductInfo> productList = [];
  List<CompDataInfo> compDataList = [];
  int orderVolume = 0;
  int compIndex = -1;
  int prodIndex = -1;

  //region InitState
  @override
  void initState() {
    super.initState();

    //region GetProductFromOrder
    widget.orderList.forEach((item) {
      var product = new ProductInfo();
      product.docNo = item.docNo;
      product.doNo = item.doNo;
      product.itemNo = item.itemNo;
      product.qty = item.qty;
      product.unit = item.unit;
      product.productId = item.productId;
      product.productCode = item.productCode;
      product.productColor = item.productColor;
      product.shipTo = item.shipTo;

      productList.add(product);
      orderVolume += product.qty;
    });
    //endregion

    //region GetCompartmentFromTuComp
    TempData.tuCompList.forEach((item) {
      var compData = new CompDataInfo();
      compData.compNo = item.compNo;
      compData.tuNo = item.tuNo;
      compData.qty = 0;
      compData.compMax = item.compMax;
      compData.compColor = "";
      compData.productCode = "";
      compData.shipTo = "";

      compDataList.add(compData);
    });
    //endregion
  }

  //endregion

  //WidgetView
  @override
  Widget build(BuildContext context) {
    //region ProductContent
    var productContent = Padding(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      child: GridView.count(
        crossAxisCount: 3,
        children: List<Widget>.generate(productList.length, (index) {
          return InkWell(
            child: Card(
              color: productList[index].productColor.isEmpty
                  ? Colors.grey[400]
                  : Color(int.parse(productList[index].productColor)),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              elevation: 3,
              child: Padding(
                padding: EdgeInsets.all(4.0),
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      TempData.deliveryType == DeliveryType.SendByPtt
                          ? productList[index].doNo
                          : productList[index].docNo,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12.0,
                      ),
                    ),
                    SizedBox(height: 4.0),
                    Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: Colors.white,
                            width: 0.5,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 4.0),
                    Text(
                      productList[index].productCode +
                          " : " + productList[index].qty.toString(),
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                    ),
                    SizedBox(height: 4.0),
                    Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: Colors.white,
                            width: 0.5,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 2.0),
                    Text(
                      productList[index].shipTo,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                    ),
                  ],
                ),
              ),
            ),
            onTap: () => {},
          );
        }),
      ),
    );
    //endregion

    //region CompartmentContent
    var compContent = Padding(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      child: GridView.count(
        crossAxisCount: 3,
        children: List<Widget>.generate(compDataList.length, (index) {
          return InkWell(
            child: Card(
              color: compDataList[index].compColor.isEmpty
                  ? Colors.grey[400]
                  : Color(int.parse(compDataList[index].compColor)),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              elevation: 3,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 3.0, vertical: 2.0),
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  //mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text(
                      "ช่อง " +
                          compDataList[index].compNo +
                          " : " +
                          compDataList[index].compMax.toString(),
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                    ),
                    SizedBox(height: 4.0),
                    Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: Colors.white,
                            width: 0.5,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 4.0),
                    Text(
                      compDataList[index].qty > 0
                          ? compDataList[index].productCode +
                              " : " + compDataList[index].qty.toString()
                          : "0",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                    ),
                    SizedBox(height: 4.0),
                    Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: Colors.white,
                            width: 0.5,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 2.0),
                    Text(
                      compDataList[index].shipTo,
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 12.0),
                    ),
                  ],
                ),
              ),
            ),
            onTap: () {
              showDialog(
                  barrierDismissible: false,
                  context: context,
                  builder: (BuildContext context) {
                    //region GetCompProduct
                    ///Get compartment index
                    compIndex = index;

                    ///Get default qty used
                    txtQtyUsed.text = compDataList[index].qty.toString();

                    ///Get product
                    if (compDataList[index].qty == 0) {
                      for (int i = 0; i < productList.length; i++) {
                        if (productList[i].qty > 0) {
                          //Add new product into compartment
                          compDataList[index].docNo = productList[i].docNo;
                          compDataList[index].doNo = productList[i].doNo;
                          compDataList[index].itemNo = productList[i].itemNo;
                          compDataList[index].unit = productList[i].unit;
                          compDataList[index].productId = productList[i].productId;
                          compDataList[index].productCode = productList[i].productCode;

                          //Get product qty
                          prodIndex = i;
                          availableQty = productList[i].qty;
                          break;
                        } else {
                          prodIndex = -1;
                          availableQty = 0;
                        }
                      }
                    } else {
                      //Get product qty by compartment
                      var docNo = compDataList[index].docNo;
                      var productCode = compDataList[index].productCode;
                      prodIndex = productList.indexWhere((item) => item.docNo == docNo && item.productCode == productCode);
                      availableQty = productList[prodIndex].qty;
                    }
                    //endregion

                    //region DialogContent
                    return AlertDialog(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                      titlePadding: EdgeInsets.all(0),
                      contentPadding: EdgeInsets.all(15.0),
                      title: Container(
                        height: 50.0,
                        decoration: BoxDecoration(
                          color: Colors.blueGrey,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10.0),
                              topRight: Radius.circular(10.0)),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            "ช่องที่ " +
                                compDataList[index].compNo +
                                " / " +
                                compDataList[index].productCode,
                            style: TextStyle(
                                fontSize: App.fontSize,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                      ),
                      content: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          SizedBox(height: 10.0),
                          InputField(
                            fieldName: txtQtyUsed,
                            fontSize: 45.0,
                            height: 50,
                            fontColor: Colors.blue,
                            textAlign: TextAlign.center,
                            readOnly: true,
                          ),
                          SizedBox(height: 15.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: RaisedButton(
                                  child: Text(
                                    "- 1000",
                                    style: TextStyle(fontSize: App.fontSize),
                                  ),
                                  color: Colors.grey[500],
                                  textColor: Colors.white,
                                  onPressed: () => {removeQtyComp()},
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                    side: BorderSide.none,
                                  ),
                                ),
                              ),
                              SizedBox(width: 10.0),
                              Expanded(
                                child: RaisedButton(
                                  child: Text(
                                    "+ 1000",
                                    style: TextStyle(fontSize: App.fontSize),
                                  ),
                                  color: Colors.green,
                                  textColor: Colors.white,
                                  onPressed: () => {addQtyComp()},
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                    side: BorderSide.none,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: RaisedButton(
                                  child: Text(
                                    "เคลียร์",
                                    style: TextStyle(fontSize: App.fontSize),
                                  ),
                                  color: Colors.grey[500],
                                  textColor: Colors.white,
                                  onPressed: () => {clearQtyComp()},
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                    side: BorderSide.none,
                                  ),
                                ),
                              ),
                              SizedBox(width: 10.0),
                              Expanded(
                                child: RaisedButton(
                                  child: Text(
                                    "เต็มช่อง",
                                    style: TextStyle(fontSize: App.fontSize),
                                  ),
                                  color: Colors.green,
                                  textColor: Colors.white,
                                  onPressed: () => {addQtyFullComp()},
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                    side: BorderSide.none,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 30.0),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: RaisedButton(
                                  child: Text(
                                    "ตกลง",
                                    style: TextStyle(fontSize: App.fontSize),
                                  ),
                                  color: Colors.blue,
                                  textColor: Colors.white,
                                  onPressed: () => {confirmQtyComp()},
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                    side: BorderSide.none,
                                  ),
                                ),
                              ),
                              SizedBox(width: 10.0),
                              Expanded(
                                child: RaisedButton(
                                  child: Text(
                                    "ยกเลิก",
                                    style: TextStyle(fontSize: App.fontSize),
                                  ),
                                  color: Colors.redAccent,
                                  textColor: Colors.white,
                                  onPressed: () => {cancelQtyComp()},
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(30.0),
                                    side: BorderSide.none,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    );
                    //endregion
                  });
            },
          );
        }),
      ),
    );
    //endregion

    //region MainContentView
    return Scaffold(
      appBar: AppBar(
        leading: AppBarBackIcon(),
        title: Text("การจัดช่องรถ"),
      ),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            height: 35.0,
            color: Colors.blueGrey,
            child: Text(
              "ผลิตภัณฑ์ " + productList.length.toString() + " รายการ",
              style: TextStyle(
                  fontSize: App.fontSize,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
          ),
          SizedBox(height: 5.0),
          Expanded(
            child: productContent,
          ),
          SizedBox(height: 15.0),
          Container(
            alignment: Alignment.center,
            height: 35.0,
            color: Colors.blueGrey,
            child: Text(
              "รถทะเบียน " + TempData.vehicleTuInfo.vehicleTuNo
                  + " / จำนวน " + compDataList.length.toString() + " ช่อง",
              style: TextStyle(
                  fontSize: App.fontSize,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
          ),
          SizedBox(height: 5.0),
          Expanded(
            child: compContent,
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              if (Menus.confirmComp != null)
                Flexible(
                  child: Button(
                    icon: getIcon(Menus.confirmComp.menuIcon),
                    text: Menus.confirmComp.menuName,
                    onClick: () => {confirmManualComp()},
                  ),
                ),
            ],
          ),
        ),
        color: Colors.transparent,
        elevation: 0,
      ),
    );
    //endregion
  }

  //region AddQtyComp
  void addQtyComp() async {
    try {
      setState(() {
        int qtyUsed = (int.parse(txtQtyUsed.text));

        if (compDataList[compIndex].compMax > qtyUsed) {
          if (availableQty > 0) {
            //Add qty used
            txtQtyUsed.text = (qtyUsed + 1000).toString();

            //Remove from available qty
            availableQty -= 1000;
          }
        }
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region removeQtyComp
  void removeQtyComp() async {
    try {
      setState(() {
        int qtyUsed = (int.parse(txtQtyUsed.text));

        if (qtyUsed > 0) {
          //Remove qty from used
          txtQtyUsed.text = (qtyUsed - 1000).toString();

          //Add qty back to available
          availableQty += 1000;
        }
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region AddQtyFullComp
  void addQtyFullComp() async {
    try {
      setState(() {
        int qtyUsed = (int.parse(txtQtyUsed.text));

        if (availableQty >= compDataList[compIndex].compMax) {
          //Add qty used with full compartment
          txtQtyUsed.text = compDataList[compIndex].compMax.toString();

          //Remove qty from available
          availableQty -= compDataList[compIndex].compMax;
        } else {
          ///Check available compartment
          int availableComp = compDataList[compIndex].compMax - qtyUsed;

          if (availableQty > 0 && availableComp > 0) {
            if (availableQty > availableComp) {
              //Add qty used with full compartment
              txtQtyUsed.text = (qtyUsed + availableComp).toString();

              //Remove qty from available
              availableQty -= availableComp;
            } else {
              //Add qty used
              txtQtyUsed.text = (qtyUsed + availableQty).toString();

              //Clear qty form available
              availableQty = 0;
            }
          }
        }
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

  //region ClearQtyComp
  void clearQtyComp() async {
    try {
      setState(() {
        //Add qty back to available
        availableQty += int.parse(txtQtyUsed.text);

        //Clear qty from used
        txtQtyUsed.text = "0";
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

  //region ConfirmQtyComp
  void confirmQtyComp() async {
    try {
      setState(() {
        int valChange = 0;
        int qtyUsed = (int.parse(txtQtyUsed.text));

        //region CheckCompartment
        if (qtyUsed > compDataList[compIndex].qty) {
          ///Case Add Comp
          //Add qty to compartment selected
          valChange = qtyUsed - compDataList[compIndex].qty;
          compDataList[compIndex].qty += valChange;

          //Remove total qty from product selected
          if (prodIndex >= 0) {
            productList[prodIndex].qty -= valChange;
          }
        } else {
          ///Case Remove Comp
          //Remove qty from compartment selected
          valChange = compDataList[compIndex].qty - qtyUsed;
          compDataList[compIndex].qty -= valChange;

          //Clear product from compartmnet when qty = 0
          if (compDataList[compIndex].qty == 0) {
            clearCompProduct();
          }

          //Add qty back to product selected
          if (prodIndex >= 0) {
            productList[prodIndex].qty += valChange;
          }
        }
        //endregion

        //region SetCompColorAndShipTo
        ///Set compartment color and shipTo
        if (compDataList[compIndex].qty > 0 &&
            compDataList[compIndex].compColor == "") {
          compDataList[compIndex].compColor =
              widget.orderList[prodIndex].productColor;
          compDataList[compIndex].shipTo = widget.orderList[prodIndex].shipTo;
        }

        ///Set product color
        if (prodIndex >= 0) {
          if (productList[prodIndex].qty > 0 &&
              productList[prodIndex].productColor == "") {
            productList[prodIndex].productColor =
                widget.orderList[prodIndex].productColor;
          } else if (productList[prodIndex].qty == 0) {
            productList[prodIndex].productColor = "";
          }
        }
        //endregion

        ///Close dialog
        Navigator.of(context).pop();
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

  //region CancelQtyComp
  void cancelQtyComp() async {
    try {
      setState(() {
        //Clear product from compartmnet when qty = 0
        if (compDataList[compIndex].qty == 0) {
          clearCompProduct();
        }

        //Close dialog
        Navigator.of(context).pop();
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

  //region ClearCompProduct
  void clearCompProduct() async {
    try {
      compDataList[compIndex].docNo = "";
      compDataList[compIndex].doNo = "";
      compDataList[compIndex].itemNo = "";
      compDataList[compIndex].unit = "";
      compDataList[compIndex].productId = "";
      compDataList[compIndex].productCode = "";
      compDataList[compIndex].compColor = "";
      compDataList[compIndex].shipTo = "";
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

  //region ConfirmManualComp
  void confirmManualComp() async {
    try {
      var inputData = new GetShipmentModel();
      inputData.deliveryType = TempData.deliveryType;
      inputData.depotInfo = TempData.depotInfo;
      inputData.vehicleInfo = TempData.vehicleInfo;
      inputData.vehicleTuInfo = TempData.vehicleTuInfo;
      inputData.driverList = TempData.driverList;
      inputData.orderList = widget.orderList;
      inputData.orderVolume = orderVolume;
      inputData.tuVolume = TempData.vehicleTuInfo.tuMaxVolume;
      inputData.compDataList = compDataList;

//      print(jsonEncode(TempData.depotInfo.toJson()));
//      print(jsonEncode(TempData.vehicleInfo.toJson()));
//      print(jsonEncode(TempData.vehicleTuInfo.toJson()));
//      print(jsonEncode(TempData.driverList));
//      print(jsonEncode(widget.orderList.map((item) => item.toJson()).toList()));
//      print(jsonEncode(compDataList.map((item) => item.toJson()).toList()));

      var data = await shipmentService.getShipmentManual(inputData);
      if (SysMessage.errorMsg.isEmpty) {
        await alertSuccess(
            context,
            "เลขการขนส่ง : " +
                data.shipmentNo +
                "\nหมายเลขซีล : " +
                data.sealNo +
                "\nซีลที่ใช้ : " +
                data.sealAmount.toString());
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => HomePage()));
      } else {
        await alertError(context, SysMessage.errorMsg);
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

}
