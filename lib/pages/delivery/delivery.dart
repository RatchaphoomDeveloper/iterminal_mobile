import 'package:flutter/material.dart';
import 'package:iterminal/app_config.dart';
import 'package:iterminal/component/dialogs.dart';
import 'package:iterminal/component/buttons.dart';
import 'package:iterminal/utility/utility.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:iterminal/pages/delivery/driver_list.dart';
import 'package:iterminal/pages/delivery/delivery_data.dart';

class DeliveryPage extends StatefulWidget {
  //Create state
  @override
  DeliveryPageState createState() => DeliveryPageState();
}

class DeliveryPageState extends State<DeliveryPage> {
  //region InitState
  @override
  void initState() {
    super.initState();
  }

  //endregion

  //WidgetView
  @override
  Widget build(BuildContext context) {
    //region DepotContent
    var depotContent = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.business,
          size: 35.0,
        ),
        SizedBox(width: 15),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "ข้อมูลคลัง",
                style: TextStyle(
                    fontSize: App.fontSize,
                    fontWeight: FontWeight.bold,
                    color: App.fontColor),
              ),
              SizedBox(height: 5.0),
              Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "รหัส : ",
                      style: TextStyle(
                          fontSize: App.fontSize, color: App.fontColor),
                    ),
                    Flexible(
                        child: Text(
                      TempData.depotInfo.depotId,
                      style:
                          TextStyle(fontSize: App.fontSize, color: Colors.blue),
                    ))
                  ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "คลัง : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.depotInfo.depotName,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
            ],
          ),
        ),
      ],
    );
    //endregion

    //region DriverContent
    var driverConten = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.person,
          size: 35.0,
        ),
        SizedBox(width: 15),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "พนักงานขับรถ",
                style: TextStyle(
                    fontSize: App.fontSize,
                    fontWeight: FontWeight.bold,
                    color: App.fontColor),
              ),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "รหัส : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.driverNo,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "ชื่อ : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.fullName,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "ผู้ขนส่ง : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.userInfo.vendorName,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
            ],
          ),
        )
      ],
    );
    //endregion

    //region VehicleContent
    var vehicleContent = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.directions_bus,
          size: 35.0,
        ),
        SizedBox(width: 15),
        Flexible(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "ข้อมูลรถ",
                style: TextStyle(
                    fontSize: App.fontSize,
                    fontWeight: FontWeight.bold,
                    color: App.fontColor),
              ),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "ทะเบียนหัว : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.vehicleInfo.vehicleNo,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "ทะเบียนหาง : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.vehicleTuInfo.vehicleTuNo,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "ประเภท : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.vehicleInfo.vehicleTypeName,
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
              Row(children: <Widget>[
                Text(
                  "จำนวนช่อง : ",
                  style:
                      TextStyle(fontSize: App.fontSize, color: App.fontColor),
                ),
                Flexible(
                    child: Text(
                  TempData.tuCompList.length.toString(),
                  style: TextStyle(fontSize: App.fontSize, color: Colors.blue),
                ))
              ]),
              SizedBox(height: 5.0),
            ],
          ),
        ),
      ],
    );
    //endregion

    //region MainContentView
    return Scaffold(
      appBar: AppBar(
        leading: AppBarBackIcon(),
        title: Text("เลือกการขนส่ง"),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.all(5.0),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(children: <Widget>[
              depotContent,
              SizedBox(height: 15),
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.blue,
                      width: 1.0,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
              vehicleContent,
              SizedBox(height: 15),
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.blue,
                      width: 1.0,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
              driverConten,
              SizedBox(height: 15),
            ]),
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              if (Menus.sendByPtt != null)
                Flexible(
                  child: Button(
                    icon: getIcon(Menus.sendByPtt.menuIcon),
                    text: Menus.sendByPtt.menuName,
                    onClick: () => {onDeliverySelected(Menus.sendByPtt.menuId)},
                  ),
                ),
              SizedBox(width: 10),
              if (Menus.getByCustomer != null)
                Flexible(
                  child: Button(
                    icon: getIcon(Menus.getByCustomer.menuIcon),
                    text: Menus.getByCustomer.menuName,
                    bgColor: Colors.green,
                    onClick: () =>
                        {onDeliverySelected(Menus.getByCustomer.menuId)},
                  ),
                ),
            ],
          ),
        ),
        color: Colors.transparent,
        elevation: 0,
      ),
    );
    //endregion
  }

  //region OnDeliverySelected
  void onDeliverySelected(menuSelected) async {
    try {
      //Add driver from login user
      TempData.driverList = [];
      TempData.driverList.add(TempData.userInfo.driverNo);

      //Check menu selected
      if (menuSelected == Menus.menuSendByPttID) {
        //Keep delivery type into temp
        TempData.deliveryType = DeliveryType.SendByPtt;
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => DriverListPage()));
      } else {
        //Keep delivery type into temp
        TempData.deliveryType = DeliveryType.GetByCustomer;
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => DeliveryDataPage()));
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

}
