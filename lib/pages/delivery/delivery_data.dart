import 'package:flutter/material.dart';
import 'package:iterminal/app_config.dart';
import 'package:iterminal/pages/home.dart';
import 'package:iterminal/component/dialogs.dart';
import 'package:iterminal/models/order_model.dart';
import 'package:iterminal/pages/delivery/manual_comp.dart';
import 'package:iterminal/services/order_service.dart';
import 'package:iterminal/component/buttons.dart';
import 'package:iterminal/utility/utility.dart';
import 'package:iterminal/models/shipment_model.dart';
import 'package:iterminal/services/shipment_service.dart';
import 'package:iterminal/component/input_field.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'dart:convert';

class DeliveryDataPage extends StatefulWidget {
//  final DepotInfo depotInfo;
//  final VehicleInfo vehicleInfo;
//
//  DeliveryDataPage({this.depotInfo, this.vehicleInfo});

  //Create state
  @override
  DeliveryDataPageState createState() => DeliveryDataPageState();
}

class DeliveryDataPageState extends State<DeliveryDataPage> {
  List<OrderInfo> orderList = [];
  var txtSearchDocNo = new TextEditingController();
  var txtMessage = new TextEditingController();
  var docList = [];

  //region InitState
  @override
  void initState() {
    super.initState();

    //Get order
    if (TempData.deliveryType == DeliveryType.SendByPtt) {
      getOrderList();
    }
  }

  //endregion

  //WidgetView
  @override
  Widget build(BuildContext context) {
    //region DialogContent
    var dialogContent = AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      titlePadding: EdgeInsets.all(0),
      contentPadding: EdgeInsets.all(15.0),
      title: Container(
        height: 50.0,
        decoration: BoxDecoration(
          color: Colors.blueGrey,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10.0), topRight: Radius.circular(10.0)),
        ),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          Text(
            "เพิ่มข้อมูลการขนส่ง",
            style: TextStyle(
                fontSize: App.fontSize,
                fontWeight: FontWeight.bold,
                color: Colors.white),
          ),
        ]),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          InputFieldExtra(
            fieldName: txtSearchDocNo,
            icon: LineAwesomeIcons.search,
            textHint: "เลขที่เอกสาร",
            bgColor: Colors.blueGrey[200],
            //onChange: (value) => {searchText = value}
          ),
          InputField(
            fieldName: txtMessage,
            fontColor: Colors.blue,
            textAlign: TextAlign.center,
            readOnly: true,
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: RaisedButton(
                  child: Text(
                    "เพิ่มข้อมูล",
                    style: TextStyle(fontSize: App.fontSize),
                  ),
                  color: Colors.blue,
                  textColor: Colors.white,
                  onPressed: () => {getOrderList()},
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0),
                    side: BorderSide.none,
                  ),
                ),
              ),
              SizedBox(width: 10.0),
              Expanded(
                child: RaisedButton(
                  child: Text(
                    "ปิด",
                    style: TextStyle(fontSize: App.fontSize),
                  ),
                  color: Colors.redAccent,
                  textColor: Colors.white,
                  onPressed: () {
                    txtSearchDocNo.text = "";
                    txtMessage.text = "";
                    Navigator.of(context).pop();
                  },
                  shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0),
                    side: BorderSide.none,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
    //endregion

    //region MainContentView
    return Scaffold(
      appBar: AppBar(
        leading: AppBarBackIcon(),
        title: Text("ข้อมูลการขนส่ง"),
        actions: <Widget>[
          if (TempData.deliveryType == DeliveryType.GetByCustomer)
            IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
                size: 30,
              ),
              onPressed: () {
                showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: (BuildContext context) {
                      return dialogContent;
                    });
              },
            ),
        ],
      ),
      body: Column(
        children: <Widget>[
          SizedBox(height: 10.0),
          Expanded(
            child: ListView.builder(
              itemCount: orderList.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: const EdgeInsets.all(5.0),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Column(
                      //crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                            //crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              ///Show DoNo/DocNo with first item only.
                              if (orderList[index].itemNo == "1" ||
                                  orderList[index].itemNo == "10")
                                Text(
                                  "คิวที่ " +
                                      orderList[index].queue +
                                      " : " +
                                      (TempData.deliveryType ==
                                              DeliveryType.SendByPtt
                                          ? orderList[index].doNo
                                          : orderList[index].docNo),
                                  style: TextStyle(
                                      fontSize: App.fontSize,
                                      color: App.fontColor,
                                      fontWeight: FontWeight.bold),
                                ),
                              Spacer(),

                              ///Show select icon if send by ptt.
                              ///And show icon only first row of queue.
                              if (TempData.deliveryType ==
                                      DeliveryType.SendByPtt &&
                                  (orderList[index].itemNo == "1" ||
                                      orderList[index].itemNo == "10") &&
                                  (index == 0
                                      ? orderList[index].queue.isNotEmpty
                                      : orderList[index].queue !=
                                          orderList[index - 1].queue))
                                InkWell(
                                  child: Icon(
                                    orderList[index].isSelected == true
                                        ? Icons.check_circle
                                        : Icons.add_circle,
                                    size: 32.0,
                                    color: orderList[index].isSelected == true
                                        ? Colors.green
                                        : Colors.orange,
                                  ),
                                  onTap: () =>
                                      {manageOrderSendByPtt(orderList[index])},
                                ),

                              ///Show delete icon if get by customer.
                              if (TempData.deliveryType ==
                                      DeliveryType.GetByCustomer &&
                                  (orderList[index].itemNo == "1" ||
                                      orderList[index].itemNo == "10"))
                                InkWell(
                                  child: Icon(
                                    Icons.remove_circle,
                                    size: 32.0,
                                    color: Colors.redAccent,
                                  ),
                                  onTap: () => {
                                    manageOrderGetByCustomer(orderList[index])
                                  },
                                ),
                            ]),
                        SizedBox(height: 5.0),
                        Row(children: <Widget>[
                          Text(
                            "ผลิตภัณฑ์ : ",
                            style: TextStyle(
                                fontSize: App.fontSize, color: App.fontColor),
                          ),
                          Flexible(
                              child: Text(
                            orderList[index].productCode,
                            style: TextStyle(
                                fontSize: App.fontSize, color: Colors.blue),
                          ))
                        ]),
                        SizedBox(height: 5.0),
                        Row(children: <Widget>[
                          Text(
                            "สถานที่ส่ง : ",
                            style: TextStyle(
                                fontSize: App.fontSize, color: App.fontColor),
                          ),
                          Flexible(
                              child: Text(
                            orderList[index].shipTo,
                            style: TextStyle(
                                fontSize: App.fontSize, color: Colors.blue),
                          ))
                        ]),
                        SizedBox(height: 5.0),
                        Row(children: <Widget>[
                          Text(
                            "ปริมาณ : ",
                            style: TextStyle(
                                fontSize: App.fontSize, color: App.fontColor),
                          ),
                          Flexible(
                              child: Text(
                            orderList[index].qty.toString(),
                            style: TextStyle(
                                fontSize: App.fontSize, color: Colors.blue),
                          ))
                        ]),
                        SizedBox(height: 15),
                        Container(
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                color: Colors.blue,
                                width: 1.0,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              if (Menus.manualComp != null)
                Flexible(
                  child: Button(
                    icon: getIcon(Menus.manualComp.menuIcon),
                    text: Menus.manualComp.menuName,
                    onClick: () => {onManualCompClick()},
                  ),
                ),
              SizedBox(width: 10),
              if (Menus.autoComp != null)
                Flexible(
                  child: Button(
                    icon: getIcon(Menus.autoComp.menuIcon),
                    text: Menus.autoComp.menuName,
                    bgColor: Colors.green,
                    onClick: () => {onAutoCompClick()},
                  ),
                ),
            ],
          ),
        ),
        color: Colors.transparent,
        elevation: 0,
      ),
    );
    //endregion
  }

  //region GetOrderList
  void getOrderList() async {
    try {
      var data = await orderService.getOrderList(
          TempData.deliveryType,
          TempData.depotInfo.depotId,
          TempData.vehicleInfo.vehicleNo,
          txtSearchDocNo.text);

      if (SysMessage.errorMsg.isEmpty) {
        setState(() {
          ///Order send by ptt
          if (TempData.deliveryType == DeliveryType.SendByPtt) {
            //Add order
            orderList = data.orderList;
          } else {
            ///Order get by customer
            txtMessage.text = "";
            if (data.orderList.length > 0) {
              //Add select order
              for (var item in data.orderList) {
                ///Check exist data in list
                var existValue = orderList.where((val) =>
                    val.docNo == item.docNo && val.itemNo == item.itemNo);
                if (existValue.isEmpty) {
                  item.isSelected = true;
                  orderList.add(item);
                } else {
                  txtMessage.text = SysMessage.DuplicateData;
                  break;
                }
              }

              //Check success
              if (txtMessage.text.isEmpty) {
                txtSearchDocNo.text = "";
                txtMessage.text = SysMessage.AddDataSuccess;
                //Navigator.of(context).pop();
              }
            } else {
              txtMessage.text = SysMessage.DataNotFound;
            }
          }
        });
      } else {
        await alertError(context, SysMessage.errorMsg);
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region ManageOrderSendByPtt
  void manageOrderSendByPtt(itemSelected) async {
    try {
      //Manage select order
      setState(() {
        orderList.forEach((item) {
          if (item.queue == itemSelected.queue) {
            if (item.isSelected == false) {
              item.isSelected = true;
            } else {
              item.isSelected = false;
            }
          }
        });
      });
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region ManageOrderGetByCustomer
  void manageOrderGetByCustomer(itemSelected) async {
    try {
      //Manage remove order
      var isAction = await alertConfirm(context,
          SysMessage.ConfirmRemoveDoc.replaceAll("DocNo", itemSelected.docNo));
      if (isAction) {
        setState(() {
          orderList.removeWhere((item) => item.docNo == itemSelected.docNo);
        });
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region OnManualCompClick
  void onManualCompClick() async {
    try {
      //Get order selected
      var orderSelected = orderList.where((item) => item.isSelected == true).toList();
      if (orderSelected.length > 0) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ManualCompPage(
                      orderList: orderSelected,
                    )));
      } else {
        //Check for display message
        var message = "";
        if (TempData.deliveryType == DeliveryType.SendByPtt) {
          if (orderList.length > 0) {
            message = SysMessage.NoneOrderSelected;
          } else {
            message = SysMessage.DataNotFound;
          }
        } else {
          message = SysMessage.NoneOrderAdded;
        }

        await alertError(context, message);
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

  //endregion

  //region OnAutoCompClick
  void onAutoCompClick() async {
    try {
      //Get order selected
      var orderSelected = orderList.where((item) => item.isSelected == true).toList();
      if (orderSelected.length > 0) {
        //Get order volume
        int orderVolume = 0;
        orderSelected.forEach((item) {
          orderVolume += item.qty;
        });

        //Manage input data
        var inputData = new GetShipmentModel();
        inputData.deliveryType = TempData.deliveryType;
        inputData.depotInfo = TempData.depotInfo;
        inputData.vehicleInfo = TempData.vehicleInfo;
        inputData.vehicleTuInfo = TempData.vehicleTuInfo;
        inputData.driverList = TempData.driverList;
        inputData.orderList = orderSelected;
        inputData.orderVolume = orderVolume;
        inputData.tuVolume = TempData.vehicleTuInfo.tuMaxVolume;

//      //Print json file
//      print(jsonEncode(TempData.depotInfo.toJson()));
//      print(jsonEncode(TempData.vehicleInfo.toJson()));
//      print(jsonEncode(TempData.vehicleTuInfo.toJson()));
//      print(jsonEncode(TempData.driverList));
//      print(jsonEncode(orderList.map((item) => item.toJson()).toList()));

        var data = await shipmentService.getShipmentAuto(inputData);
        if (SysMessage.errorMsg.isEmpty) {
          await alertSuccess(
              context,
              "เลขการขนส่ง : " +
                  data.shipmentNo +
                  "\nหมายเลขซีล : " +
                  data.sealNo +
                  "\nซีลที่ใช้ : " +
                  data.sealAmount.toString());
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => HomePage()));
        } else {
          await alertError(context, SysMessage.errorMsg);
        }
      } else {
        //Check for display message
        var message = "";
        if (TempData.deliveryType == DeliveryType.SendByPtt) {
          if (orderList.length > 0) {
            message = SysMessage.NoneOrderSelected;
          } else {
            message = SysMessage.DataNotFound;
          }
        } else {
          message = SysMessage.NoneOrderAdded;
        }

        await alertError(context, message);
      }
    } catch (ex) {
      await alertError(context, ex.toString());
    }
  }

//endregion

}
