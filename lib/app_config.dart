import 'package:flutter/material.dart';
import 'package:iterminal/models/login_model.dart';
import 'package:iterminal/models/vehicle_model.dart';
import 'package:iterminal/models/depot_model.dart';
import 'package:iterminal/models/driver_model.dart';

//region App
class App {
  //System config
  static const String AppName = "Smart iTerminal";
  static const String AppVersion = "1.0.1";
  static const double fontSize = 16;
  static Color fontColor = Colors.blueGrey[800];

  //iTMN service
  static const String iTMNService = "https://ptt-web-t05.pttplc.com/iTMNService/"; //Test
  // static const String iTMNService = "https://127.0.0.1:5006/";
  // static const String iTMNService =
  //     "https://fr-aouf.ditc.apps.ocpdev.pttdigital.com/"; //Test

  //static const String iTMNService = ""; //PRD
}
//endregion

//region TempData
class TempData {
  static String tokenKey;
  static UserData userInfo;
  static DepotInfo depotInfo;
  static VehicleInfo vehicleInfo;
  static VehicleTuInfo vehicleTuInfo;
  static List<TuCompInfo> tuCompList;
  static List<String> driverList;
  static int deliveryType;
}
//endregion

//region Menus
class Menus {
  //MenuInfo
  static List<MenuInfo> mainMenuList;
  static List<MenuInfo> settingMenuList;
  static MenuInfo sendByPtt;
  static MenuInfo getByCustomer;
  static MenuInfo confirmDriver;
  static MenuInfo manualComp;
  static MenuInfo autoComp;
  static MenuInfo confirmComp;
  static MenuInfo confirmQueue;
  static MenuInfo registerFace;

  //MenuID
  static int isActionID = 0;
  static const int menuDeliveryID = 1;
  static const int menuQueueID = 2;
  static const int menuProfileID = 3;
  static const int menuSettingID = 4;
  static const int menuLogoutID = 5;
  static const int menuSendByPttID = 6;
  static const int menuGetByCustomerID = 7;
  static const int menuConfirmDriverID = 8;
  static const int menuManualCompID = 9;
  static const int menuAutoCompID = 10;
  static const int menuConfirmCompID = 11;
  static const int menuConfirmQueueID = 12;
  static const int menuPasswordSettingID = 13;
  static const int menuAddFaceID = 14;
  static const int menuRegisterFaceID = 15;
}
//endregion

//region LoginType
class LoginType {
  static const int UserPass = 1;
  static const int Face = 2;
  static const int Fingerprint = 3;
}
//endregion

//region DeliveryType
class DeliveryType {
  static const int SendByPtt = 1;
  static const int GetByCustomer = 2;
}
//endregion

//region SystemMessage
class SysMessage {
  static String errorMsg = "";
  static const String LoadingMsg = "loading...";
  static const String InvalidUser = "กรุณากรอกข้อมูล เพื่อเข้าสู่ระบบ";
  static const String ConfirmLogout = "ต้องการออกจากระบบ ใช่หรือไม่?";
  static const String ConfirmRemoveDoc =
      "ต้องการลบเอกสารเลขที่ DocNo ใช่หรือไม่?";
  static const String AddDataSuccess = "เพิ่มข้อมูลเรียบร้อย";
  static const String DataNotFound = "ไม่พบข้อมูล";
  static const String DuplicateData =
      "ข้อมูลนี้มีอยู่แล้ว ไม่สามารถเพิ่มข้อมูลซ้ำได้";
  static const String NoneOrderAdded =
      "กรุณากดปุ่มค้นหา เพื่อเพิ่มข้อมูลการขนส่ง";
  static const String NoneOrderSelected =
      "กรุณากดปุ่มบวก (+) เพื่อเลือกรายการขนส่ง";
}
//endregion

//region HttpStatusCode
class HttpStatusCode {
  static const int Success = 200;
  static const int BadRequest = 400;
  static const int InternalServerError = 500;
}
//endregion
