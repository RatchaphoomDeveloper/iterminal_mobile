import 'package:flutter/material.dart';
import 'package:iterminal/app_config.dart';

//region InputField
class InputField extends StatelessWidget {
  final Object fieldName;
  final String textHint;
  final TextAlign textAlign;
  final double fontSize;
  final Color fontColor;
  final FontWeight fontWeight;
  final double height;
  final double width;
  final double radius;
  final Color bgColor;
  final bool typePassword;
  final bool readOnly;
  final Object onChange;

  InputField(
      {this.fieldName: "",
        this.textHint: "",
        this.textAlign: TextAlign.left,
        this.fontSize: App.fontSize,
        this.fontColor: Colors.white,
        this.fontWeight: FontWeight.normal,
        this.height: 40,
        this.width: double.maxFinite,
        this.radius: 30,
        this.bgColor: Colors.white30,
        this.typePassword: false,
        this.readOnly: false,
        this.onChange: ""});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: this.height,
      width: this.width,
      child: TextFormField(
        controller: this.fieldName == "" ? null : this.fieldName,
        onChanged: this.onChange == "" ? null : this.onChange,
        style: TextStyle(
            color: this.fontColor,
            fontSize: this.fontSize,
            fontWeight: this.fontWeight),
        decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
            //contentPadding: EdgeInsets.all(10.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(this.radius)),
              borderSide: BorderSide.none,
            ),
            hintStyle:
            TextStyle(color: this.fontColor, fontSize: this.fontSize),
            hintText: textHint,
            filled: true,
            fillColor: this.bgColor),
        textAlign: this.textAlign,
        obscureText: this.typePassword,
        readOnly: this.readOnly,
      ),
    );
  }
}
//endregion

//region InputFieldExtra
class InputFieldExtra extends StatelessWidget {
  final Object fieldName;
  final String textHint;
  final TextAlign textAlign;
  final double fontSize;
  final Color fontColor;
  final FontWeight fontWeight;
  final IconData icon;
  final Color iconColor;
  final double iconSize;
  final double height;
  final double width;
  final double radius;
  final Color bgColor;
  final bool typePassword;
  final bool readOnly;
  final Object onChange;

  InputFieldExtra(
      {this.fieldName: "",
        this.textHint: "",
        this.textAlign: TextAlign.left,
        this.fontSize: App.fontSize,
        this.fontColor: Colors.white,
        this.fontWeight: FontWeight.normal,
        this.icon: Icons.edit,
        this.iconColor: Colors.white,
        this.iconSize: 22,
        this.height: 40,
        this.width: double.maxFinite,
        this.radius: 30,
        this.bgColor: Colors.white30,
        this.typePassword: false,
        this.readOnly: false,
        this.onChange: ""});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: this.height,
      width: this.width,
      child: TextFormField(
        controller: this.fieldName == "" ? null : this.fieldName,
        onChanged: this.onChange == "" ? null : this.onChange,
        style: TextStyle(
            color: fontColor,
            fontSize: this.fontSize,
            fontWeight: this.fontWeight),
        decoration: InputDecoration(
            contentPadding: EdgeInsets.all(10.0),
            prefixIcon: Icon(
              this.icon,
              color: this.iconColor,
              size: this.iconSize,
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(this.radius)),
              borderSide: BorderSide.none,
            ),
            hintStyle:
            TextStyle(color: this.fontColor, fontSize: this.fontSize),
            hintText: this.textHint,
            filled: true,
            fillColor: this.bgColor),
        textAlign: this.textAlign,
        obscureText: this.typePassword,
        readOnly: this.readOnly,
      ),
    );
  }
}
//endregion
