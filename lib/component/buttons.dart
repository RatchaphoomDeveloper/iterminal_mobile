import 'package:flutter/material.dart';
import 'package:iterminal/app_config.dart';

//region Button
class Button extends StatelessWidget {
  final String text;
  final double fontSize;
  final Color fontColor;
  final FontWeight fontWeight;
  final IconData icon;
  final Color iconColor;
  final double iconSize;
  final double width;
  final double height;
  final double radius;
  final Color bgColor;
  final Object onClick;

  Button(
      {this.text: "",
      this.fontSize: App.fontSize,
      this.fontColor: Colors.white,
      this.fontWeight: FontWeight.normal,
      this.icon: Icons.edit,
      this.iconColor: Colors.white,
      this.iconSize: 22,
      this.width: double.maxFinite,
      this.height: 40,
      this.radius: 30,
      this.bgColor: Colors.blue,
      this.onClick: ""});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: this.height,
        width: this.width,
        child: RaisedButton.icon(
            color: this.bgColor,
            onPressed: this.onClick == "" ? () {} : this.onClick,
            icon: Icon(
              this.icon,
              color: this.iconColor,
              size: this.iconSize,
            ),
            label: Text(
              this.text,
              style: TextStyle(
                  color: this.fontColor,
                  fontSize: this.fontSize,
                  fontWeight: this.fontWeight),
            ),
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(this.radius),
              side: BorderSide.none,
            )));
  }
}
//endregion

//region ButtonExtra
//class ButtonExtra extends StatelessWidget {
//  final String text;
//  final double fontSize;
//  final Color fontColor;
//  final FontWeight fontWeight;
//  final IconData icon;
//  final Color iconColor;
//  final double iconSize;
//  final double width;
//  final double height;
//  final double radius;
//  final Color bgColor;
//  final bool hasNavigation;
//
//  ButtonExtra(
//      {this.text: "",
//      this.fontSize: App.fontSize,
//      this.fontColor: Colors.white,
//      this.fontWeight: FontWeight.normal,
//      this.icon: Icons.edit,
//      this.iconColor: Colors.white,
//      this.iconSize: 25,
//      this.width: double.maxFinite,
//      this.height: 50,
//      this.radius: 30,
//      this.bgColor: Colors.black38,
//      this.hasNavigation: true});
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      height: this.height,
//      width: this.width,
//      margin: EdgeInsets.symmetric(
//        horizontal: 10,
//      ).copyWith(
//        bottom: 15,
//      ),
//      padding: EdgeInsets.symmetric(
//        horizontal: 20,
//      ),
//      decoration: BoxDecoration(
//        borderRadius: BorderRadius.circular(this.radius),
//        color: this.bgColor,
//      ),
//      child: Row(
//        children: <Widget>[
//          Icon(
//            this.icon,
//            size: this.iconSize,
//            color: this.iconColor,
//          ),
//          SizedBox(width: 15),
//          Text(
//            this.text,
//            style: TextStyle(
//                color: this.fontColor,
//                fontSize: this.fontSize,
//                fontWeight: this.fontWeight),
//          ),
//          Spacer(),
//          if (this.hasNavigation)
//            Icon(
//              LineAwesomeIcons.angle_right,
//              size: this.iconSize,
//              color: this.iconColor,
//            ),
//        ],
//      ),
//    );
//  }
//}
//endregion

//region AppBarBackIcon
class AppBarBackIcon extends StatelessWidget {
  final IconData icon;
  final Color iconColor;
  final double iconSize;
  final Object targetPage;

  AppBarBackIcon(
      {
        this.icon: Icons.arrow_back_ios,
        this.iconColor: Colors.white,
        this.iconSize: 22,
        this.targetPage: ""});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        this.icon,
        color: this.iconColor,
        size: this.iconSize,
      ),
      onPressed: () => this.targetPage == ""
          ? Navigator.of(context).pop()
          : Navigator.push(context, MaterialPageRoute(builder: (context) => this.targetPage)),
    );
  }
}
//endregion
