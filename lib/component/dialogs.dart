import 'package:flutter/material.dart';
import 'package:iterminal/app_config.dart';

//region AlertSuccess
alertSuccess(context, message) async {
  await showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        titlePadding: EdgeInsets.all(0),
        title: Container(
            height: 40.0,
            decoration: BoxDecoration(
              color: Colors.green,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5.0),
                  topRight: Radius.circular(5.0)),
            ),
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.check_circle,
                      color: Colors.white,
                      size: 25.0,
                    ),
                    SizedBox(width: 5.0),
                    Text(
                      "สำเร็จ",
                      style: TextStyle(
                          fontSize: App.fontSize, color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ],
                ))),
        content: Text(
          message,
          style: TextStyle(fontSize: App.fontSize, color: App.fontColor),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text("ตกลง", style: TextStyle(fontSize: App.fontSize)),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
//endregion

//region AlertConfirm
alertConfirm(context, message) async {
  var isAction = false;

  await showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        titlePadding: EdgeInsets.all(0),
        title: Container(
            height: 40.0,
            decoration: BoxDecoration(
              color: Colors.amber,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5.0),
                  topRight: Radius.circular(5.0)),
            ),
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.info,
                      color: Colors.white,
                      size: 25.0,
                    ),
                    SizedBox(width: 5.0),
                    Text(
                      "การยืนยัน",
                      style: TextStyle(
                          fontSize: App.fontSize, color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ],
                ))),
        content: Text(
          message,
          style: TextStyle(fontSize: App.fontSize, color: App.fontColor),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text("ตกลง", style: TextStyle(fontSize: App.fontSize)),
            onPressed: () {
              Navigator.of(context).pop();
              isAction = true;
            },
          ),
          FlatButton(
            child: Text("ยกเลิก", style: TextStyle(fontSize: App.fontSize)),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );

  return isAction;
}
//endregion

//region AlertError
alertError(context, message) async {
  await showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        titlePadding: EdgeInsets.all(0),
        title: Container(
            height: 40.0,
            decoration: BoxDecoration(
              color: Colors.redAccent,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5.0),
                  topRight: Radius.circular(5.0)),
            ),
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.error,
                      color: Colors.white,
                      size: 25.0,
                    ),
                    SizedBox(width: 5.0),
                    Text(
                      "ไม่สำเร็จ",
                      style: TextStyle(
                          fontSize: App.fontSize, color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ],
                ))),
        content: Text(
          message,
          style: TextStyle(fontSize: App.fontSize, color: App.fontColor),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text("ตกลง", style: TextStyle(fontSize: App.fontSize)),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
//endregion

//region AlertWarning
//alertWarning(context, message) async {
//  await showDialog(
//    barrierDismissible: false,
//    context: context,
//    builder: (BuildContext context) {
//      return AlertDialog(
//        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
//        titlePadding: EdgeInsets.all(0),
//        title: Container(
//            height: 40.0,
//            decoration: BoxDecoration(
//              color: Colors.lime,
//              borderRadius: BorderRadius.only(
//                  topLeft: Radius.circular(5.0),
//                  topRight: Radius.circular(5.0)),
//            ),
//            child: Padding(
//                padding: EdgeInsets.symmetric(horizontal: 10.0),
//                child: Row(
//                  children: <Widget>[
//                    Icon(
//                      Icons.warning,
//                      color: Colors.white,
//                      size: 25.0,
//                    ),
//                    SizedBox(width: 5.0),
//                    Text(
//                      "แจ้งเตือน",
//                      style: TextStyle(
//                          fontSize: App.fontSize, color: Colors.white, fontWeight: FontWeight.bold),
//                    ),
//                  ],
//                ))),
//        content: Text(
//          message,
//          style: TextStyle(fontSize: App.fontSize, color: App.fontColor),
//        ),
//        actions: <Widget>[
//          FlatButton(
//            child: Text("ตกลง", style: TextStyle(fontSize: App.fontSize)),
//            onPressed: () {
//              Navigator.of(context).pop();
//            },
//          ),
//        ],
//      );
//    },
//  );
//}
//endregion
