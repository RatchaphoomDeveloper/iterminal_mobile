//region GetDriverListModels
//region OutputModel
class GetDriverListModel {
  List<DriverInfo> driverList;

  GetDriverListModel({this.driverList});

  //Convert json to object
  GetDriverListModel.fromJson(Map<String, dynamic> json) {
    if (json['DriverList'] != null) {
      driverList = new List<DriverInfo>();
      json['DriverList'].forEach((v) {
        driverList.add(new DriverInfo.fromJson(v));
      });
    }
  }

//  //Convert object to json
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    if (this.driverList != null) {
//      data['DriverList'] = this.driverList.map((v) => v.toJson()).toList();
//    }
//    return data;
//  }
}

class DriverInfo {
  String driverNo;
  String firstName;
  String lastName;
  String fullName;
  String idCard;
  String driverStatusName;
  String driverImage;
  String cardExpideDate;
  String gaslicenseNo;
  String gaslicenseValidTo;
  String oillicenseNo;
  String oillicenseValidTo;
  bool isSelected;

  DriverInfo(
      {this.driverNo,
      this.firstName,
      this.lastName,
      this.fullName,
      this.idCard,
      this.driverStatusName,
      this.driverImage,
      this.cardExpideDate,
      this.gaslicenseNo,
      this.gaslicenseValidTo,
      this.oillicenseNo,
      this.oillicenseValidTo,
      this.isSelected});

  DriverInfo.fromJson(Map<String, dynamic> json) {
    driverNo = json['DriverNo'] ?? "";
    firstName = json['FirstName'] ?? "";
    lastName = json['LastName'] ?? "";
    fullName = (json["FirstName"] ?? "") + " " + (json["LastName"] ?? "");
    idCard = json['IDCard'] ?? "";
    driverStatusName = json['DriverStatusName'] ?? "";
    driverImage = json['DriverImage'] ?? "";
    cardExpideDate = json['CardExpideDate'] ?? "";
    gaslicenseNo = json['GaslicenseNo'] ?? "";
    gaslicenseValidTo = json['GaslicenseValidTo'] ?? "";
    oillicenseNo = json['OillicenseNo'] ?? "";
    oillicenseValidTo = json['OillicenseValidTo'] ?? "";
    isSelected = false;
  }

/*
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DriverNo'] = this.driverNo;
    data['FirstName'] = this.firstName;
    data['LastName'] = this.lastName;
    data['IDCard'] = this.idCard;
    data['DriverStatusName'] = this.driverStatusName;
    data['DriverImage'] = this.driverImage;
    return data;
  }*/
}
//endregion
//endregion

//region CheckDriverDataModels
//region InputModel
class CheckDriverData {
  String driverNo;
  String driverName;
  String depotType;
  int sequent;

  CheckDriverData({
    this.driverNo,
    this.driverName,
    this.depotType,
    this.sequent,
  });

  //Convert object to json
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["DriverNo"] = this.driverNo;
    data["DriverName"] = this.driverName;
    data["DepotType"] = this.depotType;
    data["Sequent"] = this.sequent;

    return data;
  }
}
//endregion
//endregion
