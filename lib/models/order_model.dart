
//region GetOrderListModels
//region OutputModel
class GetOrderListModel {
  List<OrderInfo> orderList;

  GetOrderListModel({this.orderList});

  //Convert json to object
  GetOrderListModel.fromJson(Map<String, dynamic> json) {
    if (json['OrderList'] != null) {
      orderList = new List<OrderInfo>();
      json['OrderList'].forEach((v) {
        orderList.add(new OrderInfo.fromJson(v));
      });
    }
  }

//  //Convert object to json
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    if (this.orderList != null) {
//      data['OrderList'] = this.orderList.map((v) => v.toJson()).toList();
//    }
//    return data;
//  }
}

class OrderInfo {
  String docNo;
  String doNo;
  String docType;
  String queue;
  String itemNo;
  int qty;
  String unit;
  String soldTo;
  String shipTo;
  String transPoint;
  String receivePoint;
  String vehicleNo;
  String shipCond;
  String valt;
  String baseUnit;
  String orderStatus;
  String productId;
  String productCode;
  String productColor;
  String createDate;
  bool isSelected;

  OrderInfo(
      {this.docNo,
        this.doNo,
        this.docType,
        this.queue,
        this.itemNo,
        this.qty,
        this.unit,
        this.soldTo,
        this.shipTo,
        this.transPoint,
        this.receivePoint,
        this.vehicleNo,
        this.shipCond,
        this.valt,
        this.baseUnit,
        this.orderStatus,
        this.productId,
        this.productCode,
        this.productColor,
        this.createDate,
        this.isSelected});

  OrderInfo.fromJson(Map<String, dynamic> json) {
    docNo = json['DocNo'] ?? "";
    doNo = json['DoNo'] ?? "";
    docType = json['DocType'] ?? "";
    queue = json['Queue'] ?? "-";
    itemNo = json['ItemNo'] ?? "";
    qty = json['Qty'] ?? 0;
    unit = json['Unit'] ?? "";
    soldTo = json['SoldTo'] ?? "";
    shipTo = json['ShipTo'] ?? "";
    transPoint = json['TransPoint'] ?? "";
    receivePoint = json['ReceivePoint'] ?? "";
    vehicleNo = json['VehicleNo'] ?? "";
    shipCond = json['ShipCond'] ?? "";
    valt = json['Valt'] ?? "";
    baseUnit = json['BaseUnit'] ?? "";
    orderStatus = json['OrderStatus'] ?? "";
    productId = json['ProductId'] ?? "";
    productCode = json['ProductCode'] ?? "";
    productColor = json['ProductColor'] ?? "";
    createDate = json['CreateDate'] ?? "";
    isSelected = false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DocNo'] = this.docNo;
    data['DoNo'] = this.doNo;
    data['DocType'] = this.docType;
    data['Queue'] = this.queue;
    data['ItemNo'] = this.itemNo;
    data['Qty'] = this.qty;
    data['Unit'] = this.unit;
    data['SoldTo'] = this.soldTo;
    data['ShipTo'] = this.shipTo;
    data['TransPoint'] = this.transPoint;
    data['ReceivePoint'] = this.receivePoint;
    data['VehicleNo'] = this.vehicleNo;
    data['ShipCond'] = this.shipCond;
    data['Valt'] = this.valt;
    data['BaseUnit'] = this.baseUnit;
    data['OrderStatus'] = this.orderStatus;
    data['ProductId'] = this.productId;
    data['ProductCode'] = this.productCode;
    data['ProductColor'] = this.productColor;
    data['CreateDate'] = this.createDate;
    return data;
  }
}
//endregion
//endregion

//region ProductInfoModels
class ProductInfo {
  String docNo;
  String doNo;
  String itemNo;
  int qty;
  String unit;
  String productId;
  String productCode;
  String productColor;
  String shipTo;

  ProductInfo({
    this.docNo,
    this.doNo,
    this.itemNo,
    this.qty,
    this.unit,
    this.productId,
    this.productCode,
    this.productColor,
    this.shipTo,
  });

//  //Convert object to json
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data["DocNo"] = this.docNo;
//    data["DoNo"] = this.doNo;
//    data["ItemNo"] = this.itemNo;
//    data["TuNo"] = this.tuNo;
//    data["CompNo"] = this.compNo;
//    data["ProductId"] = this.productId;
//    data["ProductCode"] = this.productCode;
//    data["Qty"] = this.qty;
//    data["Unit"] = this.unit;
//
//    return data;
//  }
}
//endregion