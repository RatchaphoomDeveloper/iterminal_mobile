
//region GetDepotListModels
//region OutputModel
class GetDepotListModel {
  List<DepotInfo> depotList;

  GetDepotListModel({this.depotList});

  //Convert json to object
  GetDepotListModel.fromJson(Map<String, dynamic> json) {
    if (json['DepotList'] != null) {
      depotList = new List<DepotInfo>();
      json['DepotList'].forEach((v) {
        depotList.add(new DepotInfo.fromJson(v));
      });
    }
  }

//  //Convert object to json
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    if (this.depotList != null) {
//      data['DepotList'] = this.depotList.map((v) => v.toJson()).toList();
//    }
//    return data;
//  }
}

class DepotInfo {
  String depotId;
  String depotName;
  String depotStatus;
  String sealessStatus;
  String useSealStatus;
  String depotTypeId;
  String depotTypeName;

  DepotInfo(
      {this.depotId,
        this.depotName,
        this.depotStatus,
        this.sealessStatus,
        this.useSealStatus,
        this.depotTypeId,
        this.depotTypeName});

  DepotInfo.fromJson(Map<String, dynamic> json) {
    depotId = json['DepotID'] ?? "";
    depotName = json['DepotName'] ?? "";
    depotStatus = json['DepotStatus'] ?? "";
    sealessStatus = json['SealessStatus'] ?? "";
    useSealStatus = json['UseSealStatus'] ?? "";
    depotTypeId = json['DepotTypeID'] ?? "";
    depotTypeName = json['DepotTypeName'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['DepotID'] = this.depotId;
    data['DepotName'] = this.depotName;
    data['DepotStatus'] = this.depotStatus;
    data['SealessStatus'] = this.sealessStatus;
    data['UseSealStatus'] = this.useSealStatus;
    data['DepotTypeID'] = this.depotTypeId;
    data['DepotTypeName'] = this.depotTypeName;
    return data;
  }
}
//endregion
//endregion
