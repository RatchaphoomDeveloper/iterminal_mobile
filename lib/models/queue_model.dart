
//region SaveQueueModels
//region InputModel
class SaveQueueModel {
  String driverNo;
  String idCard;
  String firstName;
  String lastName;
  String vehicleNo;
  String vehicleTuNo;
  String depot;
  String vendorCode;
  String createBy;

  SaveQueueModel(
      {
        this.driverNo,
        this.idCard,
        this.firstName,
        this.lastName,
        this.vehicleNo,
        this.vehicleTuNo,
        this.depot,
        this.vendorCode,
        this.createBy,
      });

  //Convert object to json
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["DriverNo"] = this.driverNo;
    data["IDCard"] = this.idCard;
    data["FirstName"] = this.firstName;
    data["LastName"] = this.lastName;
    data["VehicleNo"] = this.vehicleNo;
    data["VehicleTuNo"] = this.vehicleTuNo;
    data["Depot"] = this.depot;
    data["VendorCode"] = this.vendorCode;
    data["CreateBy"] = this.createBy;

    return data;
  }
}
//endregion
//endregion