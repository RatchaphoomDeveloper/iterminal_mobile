//region LoginModels
//region InputModel
class LoginModel {
  int loginType;
  String idCard;
  String password;
  String base64FaceImage;

  //String fingerprint;

  LoginModel({this.loginType, this.idCard, this.password, this.base64FaceImage
      //this.fingerprint
      });

  //Convert object to json
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["LoginType"] = this.loginType;
    data["IDCard"] = this.idCard;
    data["Password"] = this.password;
    data["Base64FaceImage"] = this.base64FaceImage;
    //data["Fingerprint"] = this.fingerprint;

    return data;
  }
}
//endregion

//region OutputModel
class LoginDataModel {
  List<UserData> userData;
  List<MenuInfo> menuList;

  LoginDataModel({this.userData, this.menuList});

  //Convert json to object
  LoginDataModel.fromJson(Map<String, dynamic> json) {
    if (json["UserData"] != null) {
      userData = new List<UserData>();
      json["UserData"].forEach((v) {
        userData.add(new UserData.fromJson(v));
      });
    }
    if (json["MenuList"] != null) {
      menuList = new List<MenuInfo>();
      json["MenuList"].forEach((v) {
        menuList.add(new MenuInfo.fromJson(v));
      });
    }
  }

//  //Convert object to json
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    if (this.userData != null) {
//      data["UserData"] = this.userData.map((v) => v.toJson()).toList();
//    }
//    if (this.listMenu != null) {
//      data["Menu"] = this.listMenu.map((v) => v.toJson()).toList();
//    }
//
//    return data;
//  }
}

class UserData {
  String token;
  String driverNo;
  String firstName;
  String lastName;
  String fullName;
  String idCard;
  String idCardExpDate;
  String oilLicenseNo;
  String oilLicenseDate;
  String oilLicenseExpDate;
  String gasLicenseNo;
  String gasLicenseExpDate;
  String gasLicenseDate;
  String vendorCode;
  String vendorName;
  String driverImage;

  UserData(
      {this.token,
      this.driverNo,
      this.firstName,
      this.lastName,
      this.fullName,
      this.idCard,
      this.idCardExpDate,
      this.oilLicenseNo,
      this.oilLicenseDate,
      this.oilLicenseExpDate,
      this.gasLicenseNo,
      this.gasLicenseDate,
      this.gasLicenseExpDate,
      this.vendorCode,
      this.vendorName,
      this.driverImage});

  UserData.fromJson(Map<String, dynamic> json) {
    token = json["Token"] ?? "";
    driverNo = json["DriverNo"] ?? "";
    firstName = json["FirstName"] ?? "";
    lastName = json["LastName"] ?? "";
    fullName = (json["FirstName"] ?? "") + " " + (json["LastName"] ?? "");
    idCard = json["IDCard"] ?? "";
    idCardExpDate = json["IDCardExpDate"] ?? "";
    oilLicenseNo = json["OilLicenseNo"] ?? "";
    oilLicenseDate = json["OilLicenseDate"] ?? "";
    oilLicenseExpDate = json["OilLicenseExpDate"] ?? "";
    gasLicenseNo = json["GasLicenseNo"] ?? "";
    gasLicenseDate = json["GasLicenseDate"] ?? "";
    gasLicenseExpDate = json["GasLicenseExpDate"] ?? "";
    vendorCode = json["VendorCode"] ?? "";
    vendorName = json["VendorName"] ?? "";
    driverImage = json["DriverImage"] ?? "";
  }

/*
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["Token"] = this.token;
    data["DriverNo"] = this.driverNo;
    data["FirstName"] = this.firstName;
    data["LastName"] = this.lastName;
    data["IDCard"] = this.idCard;
    data["IDCardExpDate"] = this.idCardExpDate;
    data["OilLicenseNo"] = this.oilLicenseNo;
    data["OilLicenseDate"] = this.oilLicenseDate;
    data["OilLicenseExpDate"] = this.oilLicenseExpDate;
    data["GasLicenseNo"] = this.gasLicenseNo;
    data["GasLicenseDate"] = this.gasLicenseDate;
    data["GasLicenseExpDate"] = this.gasLicenseExpDate;
    data["VendorCode"] = this.vendorCode;
    data["VendorName"] = this.vendorName;
    data["DriverImage"] = this.driverImage;

    return data;
  }*/
}

class MenuInfo {
  int menuId;
  String menuName;
  String menuIcon;
  int menuLevel;
  int menunHeader;
  String menuType;
  int menuSequence;

  MenuInfo(
      {this.menuId,
      this.menuName,
      this.menuIcon,
      this.menuLevel,
      this.menunHeader,
      this.menuType,
      this.menuSequence});

  MenuInfo.fromJson(Map<String, dynamic> json) {
    menuId = json["MenuID"] ?? 0;
    menuName = json["MenuName"] ?? "";
    menuIcon = json["MenuIcon"] ?? "";
    menuLevel = json["MenuLevel"] ?? 0;
    menunHeader = json["MenuHeader"] ?? 0;
    menuType = json["MenuType"] ?? "";
    menuSequence = json["MenuSequence"] ?? 0;
  }

/*
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["MenuID"] = this.menuId;
    data["MenuName"] = this.menuName;
    data["MenuIcon"] = this.menuIcon;
    data["MenuLevel"] = this.menuLevel;
    data["HasNavigation"] = this.hasNavigation;

    return data;
  }*/
}
//endregion
//endregion

//region FaceDataModels
/// InputModel
class FaceDataModel {
  String driverNo;
  String driverName;
  String base64FaceImage;

  FaceDataModel({this.driverNo, this.driverName, this.base64FaceImage});

  //Convert object to json
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["driver_id"] = this.driverNo;
    data["name"] = this.driverName;
    data["b64pic"] = this.base64FaceImage;

    return data;
  }
}
//endregion
