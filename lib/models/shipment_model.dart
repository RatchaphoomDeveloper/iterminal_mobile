import 'package:iterminal/models/depot_model.dart';
import 'package:iterminal/models/vehicle_model.dart';
import 'package:iterminal/models/order_model.dart';

//region GetShipmentModels
//region InputModel
class GetShipmentModel {
  int deliveryType;
  DepotInfo depotInfo;
  VehicleInfo vehicleInfo;
  VehicleTuInfo vehicleTuInfo;
  List<String> driverList;
  List<OrderInfo> orderList;
  List<CompDataInfo> compDataList;
  int tuVolume;
  int orderVolume;

  GetShipmentModel({
    this.depotInfo,
    this.vehicleInfo,
    this.vehicleTuInfo,
    this.driverList,
    this.orderList,
    this.deliveryType,
    this.compDataList,
    this.tuVolume,
    this.orderVolume,
  });

  //Convert object to json
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["DeliveryType"] = this.deliveryType;
    data["DriverList"] = this.driverList;
    data["DepotInfo"] = this.depotInfo.toJson();
    data["VehicleInfo"] = this.vehicleInfo.toJson();
    data["VehicleTuInfo"] = this.vehicleTuInfo.toJson();
    data["OrderList"] = this.orderList.map((item) => item.toJson()).toList();
    data["TuVolume"] = this.tuVolume;
    data["OrderVolume"] = this.orderVolume;
    if (this.compDataList != null){
      data["CompDataList"] = this.compDataList.map((item) => item.toJson()).toList();
    }
    else{
      data["CompDataList"] = null;
    }

    return data;
  }
}

//endregion

//region CompDataInfoModels
class CompDataInfo {
  String docNo;
  String doNo;
  String itemNo;
  String tuNo;
  String compNo;
  String productId;
  String productCode;
  int qty;
  String unit;
  int compMax;
  String compColor;
  String shipTo;

  CompDataInfo({
    this.docNo,
    this.doNo,
    this.itemNo,
    this.tuNo,
    this.compNo,
    this.productId,
    this.productCode,
    this.qty,
    this.unit,
    this.compMax,
    this.compColor,
    this.shipTo,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["DocNo"] = this.docNo;
    data["DoNo"] = this.doNo;
    data["ItemNo"] = this.itemNo;
    data["TuNo"] = this.tuNo;
    data["CompNo"] = this.compNo;
    data["ProductId"] = this.productId;
    data["ProductCode"] = this.productCode;
    data["Qty"] = this.qty;
    data["Unit"] = this.unit;

    return data;
  }
}
//endregion

//region OutputModel
class ShipmentInfo {
  String shipmentNo;
  String sealNo;
  int sealAmount;

  ShipmentInfo({this.shipmentNo, this.sealNo, this.sealAmount});

  //Convert json to object
  ShipmentInfo.fromJson(Map<String, dynamic> json) {
    shipmentNo = json['ShipmentNo'] ?? "";
    sealNo = json['SealNo'] ?? "";
    sealAmount = json['SealAmount'] ?? 0;
  }
}
//endregion
//endregion
