
//region GetVehicleListModels
//region OutputModel
class GetVehicleListModel {
  List<VehicleInfo> vehicleList;

  GetVehicleListModel({this.vehicleList});

  //Convert json to object
  GetVehicleListModel.fromJson(Map<String, dynamic> json) {
    if (json['VehicleList'] != null) {
      vehicleList = new List<VehicleInfo>();
      json['VehicleList'].forEach((v) {
        vehicleList.add(new VehicleInfo.fromJson(v));
      });
    }
  }

//  //Convert object to json
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    if (this.vehicleList != null) {
//      data['VehicleList'] = this.vehicleList.map((v) => v.toJson()).toList();
//    }
//    return data;
//  }
}

class VehicleInfo {
  String vehicleNo;
  String vehicleDesc;
  String vehicleType;
  String vehicleTypeName;
  String vehicleStatus;
  String vehicleDepot;
  String classGroup;
  String vendorCode;

  VehicleInfo(
      {this.vehicleNo,
      this.vehicleDesc,
      this.vehicleType,
      this.vehicleTypeName,
      this.vehicleStatus,
      this.vehicleDepot,
      this.classGroup,
      this.vendorCode});

  VehicleInfo.fromJson(Map<String, dynamic> json) {
    vehicleNo = json['VehicleNo'] ?? "";
    vehicleDesc = json['VehicleDesc'] ?? "";
    vehicleType = json['VehicleType'] ?? "";
    vehicleTypeName = json['VehicleTypeName'] ?? "";
    vehicleStatus = json['VehicleStatus'] ?? "";
    vehicleDepot = json['VehicleDepot'] ?? "";
    classGroup = json['ClassGroup'] ?? "";
    vendorCode = json['VendorCode'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['VehicleNo'] = this.vehicleNo;
    data['VehicleDesc'] = this.vehicleDesc;
    data['VehicleType'] = this.vehicleType;
    data['VehicleTypeName'] = this.vehicleTypeName;
    data['VehicleStatus'] = this.vehicleStatus;
    data['VehicleDepot'] = this.vehicleDepot;
    data['ClassGroup'] = this.classGroup;
    data['VendorCode'] = this.vendorCode;

    return data;
  }
}
//endregion
//endregion

//region GetVehicleTuModels
//region OutputModel
class GetVehicleTuModel {
  List<VehicleTuInfo> vehicleTuList;
  List<TuCompInfo> tuCompList;

  GetVehicleTuModel({this.vehicleTuList, this.tuCompList});

  //Convert json to object
  GetVehicleTuModel.fromJson(Map<String, dynamic> json) {
    if (json['VehicleTuList'] != null) {
      vehicleTuList = new List<VehicleTuInfo>();
      json['VehicleTuList'].forEach((v) {
        vehicleTuList.add(new VehicleTuInfo.fromJson(v));
      });
    }

    if (json['TuCompList'] != null) {
      tuCompList = new List<TuCompInfo>();
      json['TuCompList'].forEach((v) {
        tuCompList.add(new TuCompInfo.fromJson(v));
      });
    }
  }

//  //Convert object to json
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    if (this.vehicleTuList != null) {
//      data['VehicleTuList'] =
//          this.vehicleTuList.map((v) => v.toJson()).toList();
//    }
//    return data;
//  }
}

//region VehicleTuInfo
class VehicleTuInfo {
  String vehicleNo;
  String vehicleTuNo;
  String tuItemNo;
  int tuUnlWeight;
  int tuMaxWeight;
  int tuMaxVolume;
  String volumeUnit;
  String tuComp;
  String tuStatus;
  String fixedSeal;
  String fixedSeal2;
  String lastCalibrationDate;
  String nextCalibrationDate;
  String tuLicenseNo;
  String tuLicenseDate;
  String tuLicenseExpDate;

  VehicleTuInfo(
      {this.vehicleNo,
      this.vehicleTuNo,
      this.tuItemNo,
      this.tuUnlWeight,
      this.tuMaxWeight,
      this.tuMaxVolume,
      this.volumeUnit,
      this.tuComp,
      this.tuStatus,
      this.fixedSeal,
      this.fixedSeal2,
      this.lastCalibrationDate,
      this.nextCalibrationDate,
      this.tuLicenseNo,
      this.tuLicenseDate,
      this.tuLicenseExpDate});

  VehicleTuInfo.fromJson(Map<String, dynamic> json) {
    vehicleNo = json['VehicleNo'] ?? "";
    vehicleTuNo = json['VehicleTuNo'] ?? "";
    tuItemNo = json['TuItemNo'] ?? "";
    tuUnlWeight = json['TuUnlWeight'] ?? 0;
    tuMaxWeight = json['TuMaxWeight'] ?? 0;
    tuMaxVolume = json['TuMaxVolume'] ?? 0;
    volumeUnit = json['VolumeUnit'] ?? "";
    tuComp = json['TuComp'] ?? "";
    tuStatus = json['TuStatus'] ?? "";
    fixedSeal = json['FixedSeal'] ?? "";
    fixedSeal2 = json['FixedSeal2'] ?? "";
    lastCalibrationDate = json['LastCalibrationDate'] ?? "";
    nextCalibrationDate = json['NextCalibrationDate'] ?? "";
    tuLicenseNo = json['TuLicenseNo'] ?? "";
    tuLicenseDate = json['TuLicenseDate'] ?? "";
    tuLicenseExpDate = json['TuLicenseExpDate'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['VehicleNo'] = this.vehicleNo;
    data['VehicleTuNo'] = this.vehicleTuNo;
    data['TuItemNo'] = this.tuItemNo;
    data['TuUnlWeight'] = this.tuUnlWeight;
    data['TuMaxWeight'] = this.tuMaxWeight;
    data['TuMaxVolume'] = this.tuMaxVolume;
    data['VolumeUnit'] = this.volumeUnit;
    data['TuComp'] = this.tuComp;
    data['TuStatus'] = this.tuStatus;
    data['FixedSeal'] = this.fixedSeal;
    data['FixedSeal2'] = this.fixedSeal2;
    data['LastCalibrationDate'] = this.lastCalibrationDate;
    data['NextCalibrationDate'] = this.nextCalibrationDate;
    data['TuLicenseNo'] = this.tuLicenseNo;
    data['TuLicenseDate'] = this.tuLicenseDate;
    data['TuLicenseExpDate'] = this.tuLicenseExpDate;
    return data;
  }
}
//endregion

//region TuCompInfo
class TuCompInfo {
  String tuNo;
  String compNo;
  int compMin;
  int compMax;
  int qty;
  String compColor;

  TuCompInfo({this.tuNo, this.compNo, this.compMin, this.compMax, this.qty, this.compColor});

  TuCompInfo.fromJson(Map<String, dynamic> json) {
    tuNo = json['TuNo'] ?? "";
    compNo = json['CompNo'] ?? "";
    compMin = json['CompMin'] ?? 0;
    compMax = json['CompMax'] ?? 0;
    qty = 0;
    compColor = "";
  }
}
//endregion

//region TuCompSetInfo
//class TuCompSetInfo {
//  String tuNo;
//  String compSet;
//  String compSet2;
//  String isBottom;
//  String isBottom2;
//  String fixedSeal;
//  String fixedSeal2;
//  String sealess;
//  String depot;
//
//  TuCompSetInfo({
//    this.tuNo,
//    this.compSet,
//    this.compSet2,
//    this.isBottom,
//    this.isBottom2,
//    this.fixedSeal,
//    this.fixedSeal2,
//    this.sealess,
//    this.depot,
//  });
//
//  TuCompSetInfo.fromJson(Map<String, dynamic> json) {
//    tuNo = json['TuNo'] ?? "";
//    compSet = json['CompSet'] ?? "";
//    compSet2 = json['CompSet2'] ?? "";
//    isBottom = json['IsBottom'] ?? "";
//    isBottom2 = json['IsBottom2'] ?? "";
//    fixedSeal = json['FixedSeal'] ?? "";
//    fixedSeal2 = json['FixedSeal2'] ?? "";
//    sealess = json['Sealess'] ?? "";
//    depot = json['Depot'] ?? "";
//  }
//}
//endregion

//endregion
//endregion
