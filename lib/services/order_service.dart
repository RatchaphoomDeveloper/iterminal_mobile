import 'package:iterminal/app_config.dart';
import 'package:iterminal/models/order_model.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:dio/dio.dart';
import 'dart:convert';

var orderService = new OrderService();

class OrderService {
  //region GetOrderList
  Future<GetOrderListModel> getOrderList(int deliveryType, String depot, String vehicleNo, String docNo) async {
    EasyLoading.show(status: SysMessage.LoadingMsg);
    SysMessage.errorMsg = "";
    var data;

    try {
      var dio = new Dio();
      dio.options.headers["Authorization"] = TempData.tokenKey;
      dio.options.connectTimeout = 30000; //30s
      var url = App.iTMNService + "Mobile/GetOrderList?deliveryType=$deliveryType&depot=$depot&vehicleNo=$vehicleNo&docNo=$docNo";

      var response = await dio.get(url);
      if (response.statusCode == HttpStatusCode.Success) {
        data = GetOrderListModel.fromJson(response.data);
      } else {
        if (response.statusCode == HttpStatusCode.BadRequest) {
          SysMessage.errorMsg = response.data["Message"];
        } else {
          SysMessage.errorMsg = response.statusMessage;
        }
      }
    } catch (ex) {
      SysMessage.errorMsg = ex.response.data["Message"].toString();
    }
    finally{
      EasyLoading.dismiss();
    }

    return data;
  }

  //endregion
}