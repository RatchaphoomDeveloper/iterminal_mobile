import 'package:iterminal/app_config.dart';
import 'package:iterminal/models/vehicle_model.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:dio/dio.dart';
import 'dart:convert';

var vehicleService = new VehicleService();

class VehicleService {
  //region GetVehicleList
  Future<GetVehicleListModel> getVehicleList(
      String vendorCode, String depot) async {
    EasyLoading.show(status: SysMessage.LoadingMsg);
    SysMessage.errorMsg = "";
    var data;

    try {
      var dio = new Dio();
      dio.options.headers["Authorization"] = TempData.tokenKey;
      dio.options.connectTimeout = 30000; //30s
      var url = App.iTMNService +
          "Mobile/GetVehicleList?vendorCode=$vendorCode&depot=$depot";

      var response = await dio.get(url);
      if (response.statusCode == HttpStatusCode.Success) {
        data = GetVehicleListModel.fromJson(response.data);
      } else {
        if (response.statusCode == HttpStatusCode.BadRequest) {
          SysMessage.errorMsg = response.data["Message"];
        } else {
          SysMessage.errorMsg = response.statusMessage;
        }
      }
    } catch (ex) {
      SysMessage.errorMsg = ex.response.data["Message"].toString();
    } finally {
      EasyLoading.dismiss();
    }

    return data;
  }

  //endregion

  //region GetVehicleTu
  Future<GetVehicleTuModel> getVehicleTu(String vehicleNo, String vehicleType,
      String vendorCode, String depot) async {
    EasyLoading.show(status: SysMessage.LoadingMsg);
    SysMessage.errorMsg = "";
    var data;

    try {
      var dio = new Dio();
      dio.options.headers["Authorization"] = TempData.tokenKey;
      dio.options.connectTimeout = 30000; //30s
      var url = App.iTMNService +
          "Mobile/GetVehicleTu?vehicleNo=$vehicleNo&vehicleType=$vehicleType&vendorCode=$vendorCode&depot=$depot";

      var response = await dio.get(url);
      if (response.statusCode == HttpStatusCode.Success) {
        data = GetVehicleTuModel.fromJson(response.data);
      } else {
        if (response.statusCode == HttpStatusCode.BadRequest) {
          SysMessage.errorMsg = response.data["Message"];
        } else {
          SysMessage.errorMsg = response.statusMessage;
        }
      }
    } catch (ex) {
      SysMessage.errorMsg = ex.response.data["Message"].toString();
    } finally {
      EasyLoading.dismiss();
    }

    return data;
  }

  //endregion
}
