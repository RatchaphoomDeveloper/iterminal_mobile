import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:iterminal/app_config.dart';
import 'package:iterminal/models/depot_model.dart';
import 'package:dio/dio.dart';
import 'dart:convert';

var depotService = new DepotService();

class DepotService {
  //region GetDepotList
  Future<GetDepotListModel> getDepotList(String driverNo) async {
    EasyLoading.show(status: SysMessage.LoadingMsg);
    SysMessage.errorMsg = "";
    var data;

    try {
      var dio = new Dio();
      dio.options.headers["Authorization"] = TempData.tokenKey;
      dio.options.connectTimeout = 30000; //30s
      var url = App.iTMNService + "Mobile/GetDepotList?driverNo=$driverNo";

      var response = await dio.get(url);
      if (response.statusCode == HttpStatusCode.Success) {
        data = GetDepotListModel.fromJson(response.data);
      } else {
        if (response.statusCode == HttpStatusCode.BadRequest) {
          SysMessage.errorMsg = response.data["Message"];
        } else {
          SysMessage.errorMsg = response.statusMessage;
        }
      }
    } catch (ex) {
      SysMessage.errorMsg = ex.response.data["Message"].toString();
    } finally {
      EasyLoading.dismiss();
    }

    return data;
  }
//endregion
}

//        var a = json.encode(response.data);
//        List list = json.decode(a);
//        var b = list.map((m) => DepotDataModel.fromJson(m)).toList();

//        response.data.forEach((item) {
//          var objItem = DepotDataModel.fromJson(item);
//          data.add(objItem);
//        });
