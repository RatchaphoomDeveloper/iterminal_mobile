import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:iterminal/app_config.dart';
import 'package:dio/dio.dart';
import 'package:iterminal/models/driver_model.dart';
import 'dart:convert';

var driverService = new DriverService();

class DriverService {
  //region GetDriverList
  Future<GetDriverListModel> getDriverList(String vendorCode, String depot) async {
    EasyLoading.show(status: SysMessage.LoadingMsg);
    SysMessage.errorMsg = "";
    var data;

    try {
      var dio = new Dio();
      dio.options.headers["Authorization"] = TempData.tokenKey;
      dio.options.connectTimeout = 30000; //30s
      var url = App.iTMNService + "Mobile/GetDriverList?keyword=&vendorCode=$vendorCode&depot=$depot";

      var response = await dio.get(url);
      if (response.statusCode == HttpStatusCode.Success) {
        data = GetDriverListModel.fromJson(response.data);
      } else {
        if (response.statusCode == HttpStatusCode.BadRequest) {
          SysMessage.errorMsg = response.data["Message"];
        } else {
          SysMessage.errorMsg = response.statusMessage;
        }
      }
    } catch (ex) {
      SysMessage.errorMsg = ex.response.data["Message"].toString();
    }
    finally{
      EasyLoading.dismiss();
    }

    return data;
  }
//endregion

  //region CheckDriverData
  Future<String> checkDriverData(CheckDriverData inputData) async {
    EasyLoading.show(status: SysMessage.LoadingMsg);
    SysMessage.errorMsg = "";
    var statusMsg = "";

    try {
      var dio = new Dio();
      dio.options.headers["Authorization"] = TempData.tokenKey;
      dio.options.connectTimeout = 30000; //30s
      var url = App.iTMNService + "Mobile/CheckDriverData";

      var response = await dio.post(url, data: inputData.toJson());
      if (response.statusCode == HttpStatusCode.Success) {
        statusMsg = response.data["Message"];
      } else {
        if (response.statusCode == HttpStatusCode.BadRequest) {
          SysMessage.errorMsg = response.data["Message"];
        } else {
          SysMessage.errorMsg = response.statusMessage;
        }
      }
    } catch (ex) {
      SysMessage.errorMsg = ex.response.data["Message"].toString();
    }
    finally{
      EasyLoading.dismiss();
    }

    return statusMsg;
  }
//endregion
}
