import 'package:iterminal/app_config.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:iterminal/models/queue_model.dart';
import 'package:dio/dio.dart';
import 'dart:convert';

var queueService = new QueueService();

class QueueService {
  //region SaveQueue
  Future<String> saveQueue(SaveQueueModel inputData) async {
    EasyLoading.show(status: SysMessage.LoadingMsg);
    SysMessage.errorMsg = "";
    var statusMsg = "";

    try {
      var dio = new Dio();
      dio.options.headers["Authorization"] = TempData.tokenKey;
      dio.options.connectTimeout = 30000; //30s
      var url = App.iTMNService + "Mobile/SaveQueue";

      var response = await dio.post(url, data: inputData.toJson());
      if (response.statusCode == HttpStatusCode.Success) {
        statusMsg = response.data["Message"];
      } else {
        if (response.statusCode == HttpStatusCode.BadRequest) {
          SysMessage.errorMsg = response.data["Message"];
        } else {
          SysMessage.errorMsg = response.statusMessage;
        }
      }
    } catch (ex) {
      SysMessage.errorMsg = ex.response.data["Message"].toString();
    }
    finally{
      EasyLoading.dismiss();
    }

    return statusMsg;
  }
//endregion
}
