import 'package:iterminal/app_config.dart';
import 'package:dio/dio.dart';
import 'package:iterminal/models/shipment_model.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'dart:convert';

var shipmentService = new ShipmentService();

class ShipmentService {
  //region GetShipmentAuto
  Future<ShipmentInfo> getShipmentAuto(GetShipmentModel inputData) async {
    EasyLoading.show(status: SysMessage.LoadingMsg);
    SysMessage.errorMsg = "";
    var data;

    try {
      var dio = new Dio();
      dio.options.headers["Authorization"] = TempData.tokenKey;
      dio.options.connectTimeout = 30000; //30s
      var url = App.iTMNService + "Mobile/GetShipmentAuto";

      var response = await dio.post(url, data: inputData.toJson());
      if (response.statusCode == HttpStatusCode.Success) {
        data = ShipmentInfo.fromJson(response.data);
      } else {
        if (response.statusCode == HttpStatusCode.BadRequest) {
          SysMessage.errorMsg = response.data["Message"];
        } else {
          SysMessage.errorMsg = response.statusMessage;
        }
      }
    } catch (ex) {
      SysMessage.errorMsg = ex.response.data["Message"].toString();
    }
    finally{
      EasyLoading.dismiss();
    }

    return data;
  }
//endregion

  //region GetShipmentManual
  Future<ShipmentInfo> getShipmentManual(GetShipmentModel inputData) async {
    EasyLoading.show(status: SysMessage.LoadingMsg);
    SysMessage.errorMsg = "";
    var data;

    try {
      var dio = new Dio();
      dio.options.headers["Authorization"] = TempData.tokenKey;
      dio.options.connectTimeout = 30000; //30s
      var url = App.iTMNService + "Mobile/GetShipmentManual";

      var response = await dio.post(url, data: inputData.toJson());
      if (response.statusCode == HttpStatusCode.Success) {
        data = ShipmentInfo.fromJson(response.data);
      } else {
        if (response.statusCode == HttpStatusCode.BadRequest) {
          SysMessage.errorMsg = response.data["Message"];
        } else {
          SysMessage.errorMsg = response.statusMessage;
        }
      }
    } catch (ex) {
      SysMessage.errorMsg = ex.response.data["Message"].toString();
    }
    finally{
      EasyLoading.dismiss();
    }

    return data;
  }
//endregion
}

//var json = {
//  "DeliveryType": 0,
//  "DepotInfo": {
//    "DepotID": "string",
//    "DepotName": "string",
//    "DepotStatus": "string",
//    "SealessStatus": "string",
//    "UseSealStatus": "string",
//    "DepotTypeID": "string",
//    "DepotTypeName": "string"
//  },
//  "VehicleInfo": {
//    "VehicleNo": "string",
//    "VehicleDesc": "string",
//    "VehicleType": "string",
//    "VehicleTypeName": "string",
//    "VehicleStatus": "string",
//    "VehicleDepot": "string",
//    "ClassGroup": "string",
//    "VendorCode": "string"
//  },
//  "VehicleTuList": [
//    {
//      "VehicleNo": "string",
//      "VehicleTailNo": "string",
//      "TuItemNo": "string",
//      "TuUnlWeight": "string",
//      "TuMaxWeight": "string",
//      "TuMaxVolume": "string",
//      "VolumeUnit": "string",
//      "TuComp": "string",
//      "TuStatus": "string",
//      "FixedSeal": "string",
//      "FixedSeal2": "string",
//      "LastCalibrationDate": "string",
//      "NextCalibrationDate": "string",
//      "TuLicenseNo": "string",
//      "TuLicenseDate": "string",
//      "TuLicenseExpDate": "string"
//    }
//  ],
//  "DriverList": [
//    "string"
//  ],
//  "OrderList": [
//    {
//      "DocNo": "string",
//      "DoNo": "string",
//      "DocType": "string",
//      "Queue": "string",
//      "ItemNo": "string",
//      "Qty": "string",
//      "Unit": "string",
//      "SoldTo": "string",
//      "ShipTo": "string",
//      "TransPoint": "string",
//      "ReceivePoint": "string",
//      "VehicleNo": "string",
//      "ShipCond": "string",
//      "Valt": "string",
//      "BaseUnit": "string",
//      "OrderStatus": "string",
//      "ProductId": "string",
//      "ProductCode": "string",
//      "ProductColor": "string",
//      "CreateDate": "string"
//    }
//  ],
//  "CompDataList": [
//    {
//      "DocNo": "string",
//      "DoNo": "string",
//      "ItemNo": "string",
//      "TuNo": "string",
//      "CompNo": "string",
//      "ProductId": "string",
//      "Qty": "string",
//      "Unit": "string"
//    }
//  ],
//  "SealAmount": 0,
//  "OrderVolum": 0
//};
