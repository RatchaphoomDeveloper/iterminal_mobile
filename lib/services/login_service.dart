import 'dart:io';
import 'package:dio/adapter.dart';
import 'package:iterminal/app_config.dart';
import 'package:iterminal/models/login_model.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'dart:developer' as developer;

var loginService = new LoginService();

class LoginService {
  //region GetLogin
  Future<LoginDataModel> getLogin(LoginModel inputData) async {
    EasyLoading.show(status: SysMessage.LoadingMsg);
    SysMessage.errorMsg = "";
    var data;
    var error;
    var response;
    try {
      var url = App.iTMNService + "Mobile/Login";
      BaseOptions options = new BaseOptions(
        baseUrl: url,
        connectTimeout: 3000000,
        receiveTimeout: 3000000,
        headers: {
          HttpHeaders.acceptHeader: "accept: */*",
        },
      );

      // dio.options.connectTimeout = 100000; //30s
      // dio.options.headers['content-Type'] = 'application/json';
      // dio.options.receiveTimeout = 30000; //30s
      //dio.options.receiveTimeout = 30000; //30s
      var dio = new Dio(options);

      // var apiRespon = await dio.get('${url}/Mobile/Login');
      // String str = json.encode(inputData.toJson());
      //  developer.log('log me',
      //     name: {
      //       "LoginType": inputData.loginType.toString(),
      //       "IDCard": inputData.idCard.toString(),
      //       "Password": inputData.password.toString(),
      //       "Base64FaceImage": inputData.base64FaceImage.toString()
      //     }.toString());
      if (inputData.base64FaceImage.toString() != '') {
        response = await dio.post(url, data: inputData.toJson());
        if (response.statusCode == HttpStatusCode.Success) {
          data = LoginDataModel.fromJson(response.data);
        } else {
          if (response.statusCode == HttpStatusCode.BadRequest) {
            SysMessage.errorMsg = response.data["Message"];
          } else {
            SysMessage.errorMsg = response.statusMessage;
          }
        }
      } else {
        response = await dio.post(url, data: inputData.toJson());
        if (response.statusCode == HttpStatusCode.Success) {
          data = LoginDataModel.fromJson(response.data);
        } else {
          if (response.statusCode == HttpStatusCode.BadRequest) {
            SysMessage.errorMsg = response.data["Message"];
          } else {
            SysMessage.errorMsg = response.statusMessage;
          }
        }
      }
    } catch (ex) {
      SysMessage.errorMsg = ex.response.data["Message"].toString();
    } finally {
      EasyLoading.dismiss();
    }

    return data;
  }
//endregion

  //region FaceRegister
  Future<String> faceRegister(FaceDataModel faceData) async {
    EasyLoading.show(status: SysMessage.LoadingMsg);
    SysMessage.errorMsg = "";
    var statusMsg = "";

    try {
      var dio = new Dio();
      dio.options.headers["Authorization"] = TempData.tokenKey;
      dio.options.connectTimeout = 30000; //30s
      dio.options.receiveTimeout = 30000; //30s
      dio.options.baseUrl = App.iTMNService;
      var url = App.iTMNService + "Mobile/FaceRegister";

      var response = await dio.post(url, data: faceData.toJson());
      if (response.statusCode == HttpStatusCode.Success) {
        statusMsg = response.data["Message"];
      } else {
        if (response.statusCode == HttpStatusCode.BadRequest) {
          SysMessage.errorMsg = response.data["Message"];
        } else {
          SysMessage.errorMsg = response.statusMessage;
        }
      }
    } catch (ex) {
      SysMessage.errorMsg = ex.response.data["Message"].toString();
    } finally {
      EasyLoading.dismiss();
    }

    return statusMsg;
  }
//endregion
}
